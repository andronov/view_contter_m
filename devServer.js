const http = require('http');
const path = require('path');
const webpack = require('webpack');
const express = require('express');
const DevMiddleware = require('webpack-dev-middleware');
const HotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('./webpack.config');

const {
  PORT: port = 3070,
  NODE_ENV,
  TEST_PROD
} = process.env;
const testProd = Number(TEST_PROD);
const isProduction = NODE_ENV === 'production';
const server = http.createServer();
const app = express();

if (testProd) {
  app
    .use(express.static('./dist'))
    .use('*', (req, res) => {
      res.sendFile(path.resolve('./dist/index.html'))
    })
} else {
  const compiler = webpack(webpackConfig({
    minify: false,
    prod: isProduction,
    lint: false,
    hot: true
  }));
  const devMiddleware = DevMiddleware(compiler, {
    inline: true,
    hot: true,
    noInfo: true,
    stats: { colors: true }
  });

  app
    .use(devMiddleware)
    .use(HotMiddleware(compiler))
    .use(express.static('./app'))
    .use('*', (req, res, next) => {
      devMiddleware.fileSystem
        .readFile(path.resolve('./dist/index.html'), (err, file) => {
          if (err) {
            next(err)
          } else {
            res.end(file)
          }
        })
    })
}

server
  .on('request', app)
  .listen({ port }, () => {
    console.log(`Webpack listening on port ${port}`);
  });
