import aspect from './aspect';

const cache = new WeakSet();
const replaceCircular = (k, v) => {
  if (typeof v === 'object' && v !== null) {
    if (cache.has(v)) {
      return;
    }
    cache.add(v);
  }
  return v;
};

export default fnName => {
  return aspect((result = 'undefined', args, fn) => {
    console.log(`${fnName || fn.name}() =>`,
      JSON.stringify({ result, args }, replaceCircular, 2));
  });
};
