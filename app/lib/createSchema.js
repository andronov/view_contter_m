import { createModelSchema } from 'serializr'

export default (ctor, schema) => {
  return createModelSchema(ctor, schema, (context) => {
    const store = ctor.create()
  })
}
