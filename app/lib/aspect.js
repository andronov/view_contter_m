export default aspect => {
  return fn => {
    return (...args) => {
      const result = fn(...args);
      aspect(result, args, fn);
      return result;
    };
  };
};
