import http from './http';
import { AUTH_SERVER_LOGIN_URL, AUTH_SERVER_RESET_URL, AUTH_SERVER_VERIFY_URL } from './constants';
import AuthStorage from './AuthStorage';
export function login(phone) {
  return http({
    method: 'POST',
    url: AUTH_SERVER_LOGIN_URL,
    data: {
      phone,
      device_id: AuthStorage.getSession().deviceId,
    },
  });
}
export function verifyCode(phone, sms_code) {
  return http({
    method: 'POST',
    url: AUTH_SERVER_VERIFY_URL,
    data: {
      phone,
      sms_code,
      device_id: AuthStorage.getSession().deviceId,
    },
  });
}
/**
 * Получение нового кода,спустя минуту
 * @param phone
 */
export function resetCode(phone) {
  return http({
    method: 'POST',
    url: AUTH_SERVER_RESET_URL,
    data: {
      phone,
      device_id: AuthStorage.getSession().deviceId,
    },
  });
}
