import * as React from 'react';
import { observer, inject } from 'mobx-react';
import './style.scss';

import { stickersG, layoutsG, catStickers} from './demo';
import { RestApi } from '../../services/RestApi';
import {Motion, spring} from 'react-motion';
import ReactSwipe from 'react-swipe';
import Draggable from 'react-draggable'; // The default
import {DraggableCore} from 'react-draggable'; // <DraggableCore>
import DrawCanvas from '../DrawCanvas';
import Palette from '../Palette';
import SlideRange from "../SlideRange/index";
import Item from "../Item/index";
import Tooltip from 'rc-tooltip';
import ElementItem from "../ElementItem/index";
import ImgBlur from "../ImgBlur/index";
import uuid from 'uuid';

let _id = 1;
let x = 0;
let y = 0;
let activeXPos = 0;
let activeYPos = 0;
let activeItemDivX = 0;
let activeItemDivY = 0;
let clicked = {};

@inject('appState')
@observer
export default class SharePage extends React.Component {

  state = {
    canvas: null,
    context: null,
    stickers: stickersG,
    objectsOpen: false,
    stickersOpen: false,
    elementsOpen: false,
    templatesOpen: false,
    emojiOpen: false,
    objectsOpenHeaderEmoji: true,
    objectsOpenHeaderSticker: true,
    isHeaderStickerSearch: false,
    drawMode: true,
    emojiCatSize: 'x3',
    emojiCatTone: 'tone2',
    emojiCatActive: null,
    layouts: layoutsG,
    layoutItem: null,
    emoji: [],
    emojiCat: [],
    catStickers: catStickers,
    items: [],
   /* itemLink: {"title":"Students, Meet Product Hunt – Student Voices", "description":"Hi I’m Jordan, I love helping people think & grow their products. Hope this helps! You can find me on Twitter :) First setting foot on my college campus some year and a half ago, I had no idea what I…",
      "path":"assets/media/2_6_2017/1-9Q3tZ2W1mQxaqMJpmDwoDg.gif"},

    assets/media/2_6_2017/ToMjGpr2JbrKkbDotSE*/

   itemLink: {"title":"These LED eyelashes are future fashion - The Verge", "description":"Sometimes you see a product and you’re like \"Wow, that’s so futuristic.\" Then you remember you’re living in 2017, 20 years after the date that a sentient AI wiped out most of humanity in the...",
     "path":"assets/media/3_7_2017/OEb6vuaxeV.jpg",
     "images": [
       'assets/media/2_7_2017/MH1oPWLBkn.png',
       'assets/media/2_6_2017/1-9Q3tZ2W1mQxaqMJpmDwoDg.gif',
       'assets/media/2_6_2017/4jL5DEobtg.jpg',
       'assets/media/2_6_2017/kCABk5LgyI.jpg',
       'assets/media/2_6_2017/toidIrMR5x.jpg'
     ],
     "animate": true,
   },
   // itemLink: null,
    itemLinkColors: [],
    itemLinkColor: '2c7dff',
    previewMode: false,
    brushSize: 30,
    brushColor: '#ff1d0f',
    draggableActiveItem: null,
    draggableActiveResize: false,
    draggableActiveRotate: false,
    moving: false,
    itemLoaded: false,
    itemLoading: false,
    itemItemLoading: false,
    heightObjectsStickerCat: 49,
    trashItem: null,
    linkOpen: false,
    activeHeaderSticker: 'popular'
  };

  componentDidMount() {
    let {item} = this.props;
    //this.setState({itemLink: item})
  }

  componentWillMount() {
  }


  onLoadBasicImg = (width, height) => {
    this.setState({width, height})
  };

  onClickItem = (e, item) => {
    let items = this.state.items;
    let it = items;
    it.push({id: _id, item: item, is_active: true});
    this.setState({items: it});
    _id++;
  };

  loadEmoji = () => {
    // eslint-disable-next-line global-require, import/no-dynamic-require
    let res = require(`../../emoji.json`);
    console.log('loadEmoji', res);
    let categ = [],
      cat = null,
      categories = [],
      emoji = [],
      i =  1;
    for(let key in res){
      if(categ.indexOf(res[key].category) === -1){
        categ.push(res[key].category);
        categories.push({id: i, name: res[key].category, active: i === 1, emoji: []});
        i++;
      }
      res[key].path = `/assets/emoji/png_128x128/${res[key].unicode}.png`;
      res[key].tone = null;
      if(key.endsWith('_tone1') || key.endsWith('_tone2') || key.endsWith('_tone2') ||
         key.endsWith('_tone3') || key.endsWith('_tone4') || key.endsWith('_tone5')){

        res[key.replace( /_tone[0-9]+/g, '')].tone = 'tone0';
        res[key].tone = key.match(/tone[0-9]+/g)[0];

        //console.log('res7res', key.match(/tone[0-9]+/g));
      }
      if(res[key].category !== 'modifier'){
        emoji.push(res)
      }
    }
    for(let key in res){
      cat = categories.filter(it=>it.name === res[key].category)[0];
      cat['sort'] = 20;
      if(res[key].category === 'modifier'){
        cat['sort'] = 0
      }
      if(res[key].category === 'people'){
        cat['sort'] = 1
      }
      cat.emoji.push(res[key]);
      //console.log('loadEm3oqji', cat, res[key].category)
    }
    categories[1].rendered = true;
    this.setState({emojiCat: categories, emoji: emoji, emojiCatActive: categories[1]});
    ///console.log('loadEmoqji', categories, emoji);

  };

  addSticker = () => {
    console.log('addSticker');
    this.fileInputSticker.click();
  };

  setFileInputSticker = (ref) => {
    this.fileInputSticker = ref;
  };

  onFileChangeSticker = (e) => {
    let { stickers } = this.state;
    console.log('onFileChangeSticker', e.target.files);
    let file = e.target.files[0];
    let that = this;

    if (file) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function(e) {
        let t = {
          id: Math.random(),
          uuid: null,
          path: e.target.result,
          isLoaded: false,
          cat: 1,
          popularity: 101,
        };
        stickers.push(t);
        that.setState({stickers});
       // console.log(e.target.result);
       // console.log('s8s', stickers);
      };
    }
  };

  renderStickersList = () => {
    let { stickers, catStickers, isHeaderStickerSearch, activeHeaderSticker } = this.state;
    let { appState } = this.props;
    let { size } = appState;
    let { width } = size;
    return(
      <div className='objects__template'>
        {/*this.renderObjectsStickerCat()*/}
        {/*<div onClick={this.addSticker} className='objects__add_st'>
          <input
            style={{ display: 'none' }}
            onChange={this.onFileChangeSticker}
            type='file'
            name='file'
            ref={this.setFileInputSticker}
          />
           <Tooltip placement="topLeft"
                    overlay={<div>Add sticker</div>} >
             <div className='objects__add_st_icon'/>
           </Tooltip>
        </div>*/}

        <div className='objects__top_menus'>

          <Motion key={101} style={{x: spring(isHeaderStickerSearch  ? 0 : 34), y: spring(isHeaderStickerSearch  ? 0 : 1)}}>
            {({x, y}) =>
              <div style={{width: `${x}px`, opacity: y}}
                   onClick={() => {this.setState({activeHeaderSticker: 'recent'})}}
                   className={`objects__tp_item ${activeHeaderSticker === 'recent' ? 'active': ''}`}>
                <div className='objects__tp_item_icon tp_recent_btn' />
              </div>
            }
          </Motion>

          <Motion key={102} style={{x: spring(isHeaderStickerSearch  ? 0 : 34), y: spring(isHeaderStickerSearch  ? 0 : 1)}}>
            {({x, y}) =>
              <div style={{width: `${x}px`, opacity: y}}
                   onClick={() => {this.setState({activeHeaderSticker: 'popular'})}}
                   className={`objects__tp_item ${activeHeaderSticker === 'popular' ? 'active': ''}`}>
                <div className='objects__tp_item_icon tp_pop_btn' />
              </div>
            }
          </Motion>

          <Motion key={103} style={{x: spring(isHeaderStickerSearch  ? 0 : (width-34*4)), y: spring(isHeaderStickerSearch  ? 0 : 1)}}>
            {({x, y}) =>
              <div
                onClick={() => {this.setState({activeHeaderSticker: 'category'})}}
                className={`objects__tp_item tp_cat_btn ${activeHeaderSticker === 'category' ? 'active': ''}`}
                style={{width: `${x}px`, opacity: y}} >
                Category
              </div>
            }
          </Motion>

          <Motion key={104} style={{x: spring(isHeaderStickerSearch  ? 0 : 34), y: spring(isHeaderStickerSearch  ? 0 : 1)}}>
            {({x, y}) =>
              <div style={{width: `${x}px`, opacity: y}}
                   onClick={() => {this.setState({activeHeaderSticker: 'add'})}}
                   className='objects__tp_item '>
                <div className='objects__tp_item_icon tp_add_btn' />
              </div>
            }
          </Motion>

          <Motion key={104} style={{x: spring(isHeaderStickerSearch  ? 34 : 34),
            z: spring(isHeaderStickerSearch  ? (width-34) : 0),
            o: spring(isHeaderStickerSearch  ? 0 : 1),
            a: spring(isHeaderStickerSearch  ? 1 : 0),
            y: spring(isHeaderStickerSearch  ? (width-34*2) : 0),
            x1: spring(isHeaderStickerSearch  ? 0 : 16),
            x2: spring(isHeaderStickerSearch  ? 16 : 0),
          }}>
            {({x, z, o, a, y, x1, x2}) =>
              <div style={{width: `${x}px`}}
                className='objects__tp_item '>
                {
                /*  isHeaderStickerSearch ?
                    <div className='objects__tp_item_search ' style={{width: `${z}px`}}>
                      <div
                        onClick={() => {this.setState({isHeaderStickerSearch: false})}}
                        className='objects__tp_item '>
                        <div className='objects__tp_item_icon tp_back_btn' />
                      </div>
                      <input
                        className="objects__tp_item_input"
                        style={{width: `${0}px`, opacity: o}}
                             placeholder="Search sticker"/>
                    </div>
                    :
                    null*/
                }
                <div className='objects__tp_item '>
                  <div
                    onClick={() => {this.setState({isHeaderStickerSearch: !isHeaderStickerSearch})}}
                    style={{opacity: a, width: `${x2}px`}}
                    className='objects__tp_item_icon tp_back_btn' />
                  <div
                    style={{opacity: o, width: `${x1}px`}}
                    onClick={() => {this.setState({isHeaderStickerSearch: !isHeaderStickerSearch})}}
                    className='objects__tp_item_icon tp_search_btn' />
                </div>
              </div>
            }
          </Motion>


          <Motion key={105} style={{x: spring(isHeaderStickerSearch  ? (width-34) : 0),
            y: spring(isHeaderStickerSearch  ? 1 : 0)}}>
            {({x, y}) =>
              <div style={{width: `${x}px`, opacity: y}}
                   className='objects__tp_item_search'>
                <input
                  className="objects__tp_item_input"
                  style={{opacity: y}}
                  placeholder="Search "/>
              </div>
            }
          </Motion>

        </div>

        {activeHeaderSticker === 'category' ?
          <div className='objects__items'>
            {catStickers.map(cat=>{
              return(<div key={cat.id} className='objects__item'>
                <div className='objects__item_slider'>
                  <div className='objects__item_slider_inner'>
                    {/*cat.slides.map(item=>{
                   return(
                   <div key={uuid.v4()} className='objects__item_slide'>
                   <ImgBlur width={60}
                   height={60}
                   transitionTime={100}
                   maxBlurLevel={10}
                   thumb={'w_10'}
                   ratio={true}
                   src={item.path}
                   fullCover={false}
                   item={item}/>
                   </div>
                   )
                   })*/}
                    <div key={uuid.v4()} className='objects__item_slide'>
                      <ImgBlur width={60}
                               height={60}
                               transitionTime={100}
                               maxBlurLevel={10}
                               thumb={'w_10'}
                               ratio={true}
                               src={cat.slides[0].path}
                               fullCover={false}
                               item={cat.slides[0]}/>
                    </div>
                  </div>
                </div>
                <div className='objects__item_title'>{cat.name}</div>
              </div>)
            })}
          </div>
          : null}

        {activeHeaderSticker === 'popular' ? <div className='objects__items'>
          {stickers.map(item=>{
          return(
            <div key={item.id} onClick={(e, it) => this.onClickItem(e, item)} className='objects__item'>
              <ImgBlur width={60}
                       height={60}
                       transitionTime={100}
                       maxBlurLevel={10}
                       thumb={'w_10'}
                       ratio={true}
                       src={item.path}
                       fullCover={false}
                       item={item}/>
            </div>
          )
        })}
        </div> : null}
      </div>
    )
  };

  swipeEmoji = (index, el) => {
    let { emojiCat } = this.state;
    let item = emojiCat.filter(it=>it.id.toString() === el.dataset.id.toString())[0];
    item.rendered = true;
    this.setState({emojiCatActive: item});
  };

  onClickCatEmoji = (index) => {
    this.refSwipeEmoji.slide(index);
  };

  setSwipeEmoji = (e) => {
    if(!e)return;
    this.refSwipeEmoji = e;
  };

  renderEmojisList = () => {
    let { stickers, emojiCat, emojiCatSize, emojiCatTone, emojiCatActive} = this.state;
    let activeEmojiCat = emojiCatActive ? emojiCatActive : emojiCat[1];
    let { appState } = this.props;
    let { size } = appState;
    let { width } = size;
    console.log('emojiCatActive', emojiCatActive);
    return(
    <div className='objects__template'>

      <div className='objects__top_menus'>
        {emojiCat.sort((a, b)=> a.sort-b.sort).map((item, i) => {
          let emoji = item.emoji[0];
          return (
            <div onClick={(z)=> {this.onClickCatEmoji(i)}}
                 style={{width: `${width/10}px`}}
                 className={`objects__tp_item ${emojiCatActive === item ? 'active': ''}`}>
              <ImgBlur width={((emojiCatSize === 'x1' && 190)
              || (emojiCatSize === 'x2' && 90) || (emojiCatSize === 'x3' && 16))}
                       height={((emojiCatSize === 'x1' && 190)
                       || (emojiCatSize === 'x2' && 90) || (emojiCatSize === 'x3' && 16))}
                       transitionTime={100}
                       maxBlurLevel={10}
                       ratio={true}
                       src={`/assets/emoji/${emojiCatSize === 'x1'
                       || emojiCatSize === 'x2' ? 'png_128x128' : 'png'}/${emoji.unicode}.png`}
                       fullCover={false}
                       item={emoji}/>
            </div>
          );
        })}
      </div>


        <ReactSwipe ref={this.setSwipeEmoji} className="carousel" swipeOptions={{continuous: false, callback: this.swipeEmoji}}>
          {emojiCat.map(emoji => {
            return(
              <div data-id={emoji.id} className='objects__items o_emoji'>
                {emoji.emoji.slice(0, 41).map(item => {
                  if(activeEmojiCat.name !== emoji.name && !emoji.rendered){
                    return(<div />)
                  }
                  return (
                    <div key={item.unicode}
                         onClick={(e, it) => this.onClickItem(e, item)}
                         style={{width: ((emojiCatSize === 'x1' && '190px') || (emojiCatSize === 'x2' && '90px') || (emojiCatSize === 'x3' && '32px')),
                           height: ((emojiCatSize === 'x1' && '190px') || (emojiCatSize === 'x2' && '90px') || (emojiCatSize === 'x3' && '32px'))}}
                         className='objects__item'>

                      <ImgBlur width={((emojiCatSize === 'x1' && 190)
                      || (emojiCatSize === 'x2' && 90) || (emojiCatSize === 'x3' && 32))}
                               height={((emojiCatSize === 'x1' && 190)
                               || (emojiCatSize === 'x2' && 90) || (emojiCatSize === 'x3' && 32))}
                               transitionTime={100}
                               maxBlurLevel={10}
                               ratio={true}
                               src={`/assets/emoji/${emojiCatSize === 'x1'
                               || emojiCatSize === 'x2' ? 'png_128x128' : 'png'}/${item.unicode}.png`}
                               fullCover={false}
                               item={item}/>
                    </div>
                  );
                })}
              </div>
            )
          })}
        </ReactSwipe>
      </div>
    )
  };

  renderObjectsCat = () => {
    let { emojiCat, emojiCatSize, objectsOpenHeaderEmoji, emojiCatTone, emojiCatActive } = this.state;

    return(
      <div>
        <Motion style={{x: spring(objectsOpenHeaderEmoji  ? 0 : -212)}}>
        {({x}) =>
        <div className="objects__header" style={{
                WebkitTransform: `translate3d(0, ${x}px, 0)`,
                transform: `translate3d(, ${x}px, 0)`,
              }}>
          <div className="objects__header_inner" >
            <div className="objects__header_size">
              <div className="objects__header_t">
                Size
              </div>
              <div className="objects__header_items">
                <div onClick={() => {this.setState({emojiCatSize: 'x1'})}}
                className={`objects__header_item ${emojiCatSize === 'x1' ? 'active' : ''}`}>
                     x1
                </div>
                <div onClick={() => {this.setState({emojiCatSize: 'x2'})}}
                className={`objects__header_item ${emojiCatSize === 'x2' ? 'active' : ''}`}>
                     x2
                </div>
                <div onClick={() => {this.setState({emojiCatSize: 'x3'})}}
                className={`objects__header_item ${emojiCatSize === 'x3' ? 'active' : ''}`}>
                     x3
                </div>
              </div>
            </div>
            <div className="objects__header_cat">
              <div className="objects__header_t">
                Category
              </div>
              <div className="objects__header_items">
                <div onClick={()=> {this.setState({emojiCatActive: null})}} className={`objects__header_item `}>
                  recent
                </div>
                {emojiCat.filter(v=>v.name !== 'modifier').map(item => {
                  return (
                    <div onClick={()=> {this.setState({emojiCatActive: item})}}
                         className={`objects__header_item ${emojiCatActive === item ? 'active': ''}`}>
                      {item.name}
                    </div>
                  );
                })}
                {/*emojiCat.map(item => {
                  return (
                    <div className='objects__header_item'>
                      <img src={`/assets/emoji/png/1f606.png`} />
                    </div>
                  );
                })*/}
              </div>
            </div>

            <div className="objects__header_cat">
              <div className="objects__header_t">
                Skin tone
              </div>
              <div className="objects__header_items">
                <div className={`objects__header_item_skin ${emojiCatTone === 'tone0' ? 'active': ''}`}
                     onClick={()=> {this.setState({emojiCatTone: 'tone0'})}}>
                  <img src="/assets/emoji/png/270b.png"/>
                </div>
                <div className={`objects__header_item_skin ${emojiCatTone === 'tone1' ? 'active': ''}`}
                     onClick={()=> {this.setState({emojiCatTone: 'tone1'})}}>
                  <img src="/assets/emoji/png/270b-1f3fb.png"/>
                </div>
                <div className={`objects__header_item_skin ${emojiCatTone === 'tone2' ? 'active': ''}`}
                     onClick={()=> {this.setState({emojiCatTone: 'tone2'})}}>
                  <img src="/assets/emoji/png/270b-1f3fc.png"/>
                </div>
                <div className={`objects__header_item_skin ${emojiCatTone === 'tone3' ? 'active': ''}`}
                     onClick={()=> {this.setState({emojiCatTone: 'tone3'})}}>
                  <img src="/assets/emoji/png/270b-1f3fd.png"/>
                </div>
                <div className={`objects__header_item_skin ${emojiCatTone === 'tone4' ? 'active': ''}`}
                     onClick={()=> {this.setState({emojiCatTone: 'tone4'})}}>
                  <img src="/assets/emoji/png/270b-1f3fe.png"/>
                </div>
                <div className={`objects__header_item_skin ${emojiCatTone === 'tone5' ? 'active': ''}`}
                     onClick={()=> {this.setState({emojiCatTone: 'tone5'})}}>
                  <img src="/assets/emoji/png/270b-1f3ff.png"/>
                </div>
              </div>
            </div>

          </div>

        <Motion style={{x: spring(objectsOpenHeaderEmoji  ? 0 : 180), y: spring(objectsOpenHeaderEmoji  ? 0 : 7), z: spring(objectsOpenHeaderEmoji  ? 7 : 0)}}>
        {({x, y, z}) =>
          <div onClick={() => {this.setState({objectsOpenHeaderEmoji: !this.state.objectsOpenHeaderEmoji})}}
              className="objects__header_bottom">
          <div className="objects__header_bottom_arr" style={{
                WebkitTransform: `rotate(${x}deg)`,
                transform: `rotate(${x}deg)`,
                marginBottom: `${z}px`,
                marginTop: `${y}px`
              }}/>
          <div className="objects__header_bottom_arr" style={{
                WebkitTransform: `rotate(${x}deg)`,
                transform: `rotate(${x}deg)`,
                marginBottom: `${z}px`,
                marginTop: `${y}px`
              }}/>
          </div>
          }
        </Motion>

        </div>}
        </Motion>
      </div>
      )
  };

  onClickObjects = (e, type) => {
    let { objectsOpen, emojiOpen, drawOpen} = this.state;
    let c = this.state[type];
    let dt = {emojiOpen: false, stickersOpen: false, elementsOpen: false, templatesOpen: false,
      drawOpen: false};
    let res = !c;
    dt[type] = !c;
    if(type === 'linkOpen'){objectsOpen = true;res = false}
   // dt['objectsOpen'] = (!emojiOpen && !stickersOpen && !elementsOpen) ? true : false;
    this.setState({...dt, objectsOpen: !objectsOpen});

    console.log('res', res);

    if(res){
       this.setState({objectsOpen: true});
    }

    if(type === 'drawOpen'){
      setTimeout(()=>{
        this.renderPreviewCanvas();
      }, 100);
    }

    if(!this.state.emoji.length && type === 'emojiOpen'){
      this.loadEmoji()
    }
  };

  renderElementsList = () => {
    return(
      <div className='objects__template'>
      elem
      </div>
    )
  };

  onClickLayout = (e, item) => {
    console.log('onClickLayout', item);
    this.setState({layoutItem: item});
  };

  renderTemplatesList = () => {
    let { layouts } = this.state;
    return(
      <div className='objects__template'>
        <div className='objects__items '>
        {layouts.map(item=>{
          return(
            <div key={item.id} onClick={(e, it) => this.onClickLayout(e, item)} className='objects__item'>
              <div style={{
                backgroundImage: 'url(data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMyIDMyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzMiAzMjsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI2NHB4IiBoZWlnaHQ9IjY0cHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0wLDN2MjZoMzJWM0gweiBNMzAsMjdIMlY3aDI4VjI3eiIgZmlsbD0iIzAwNmRmMCIvPgoJCTxyZWN0IHg9IjE4IiB5PSIxMSIgd2lkdGg9IjEwIiBoZWlnaHQ9IjIiIGZpbGw9IiMwMDZkZjAiLz4KCQk8cmVjdCB4PSIxOCIgeT0iMTUiIHdpZHRoPSIxMCIgaGVpZ2h0PSIyIiBmaWxsPSIjMDA2ZGYwIi8+CgkJPHJlY3QgeD0iMTgiIHk9IjE5IiB3aWR0aD0iNiIgaGVpZ2h0PSIyIiBmaWxsPSIjMDA2ZGYwIi8+CgkJPHJlY3QgeD0iNCIgeT0iMTEiIHdpZHRoPSIxMCIgaGVpZ2h0PSIxMCIgZmlsbD0iIzAwNmRmMCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=)',
                width: '64px',
                height: '64px'
              }}></div>
              {item.name}
            </div>
          )
        })}
        </div>
      </div>
    )
  };

  renderLayouts = () => {
    let {layoutItem, itemLink: item, itemLinkColors, itemLinkColor: shadowColor} = this.state;
    let cs = layoutItem.className;
    console.log('itemLinkColors', itemLinkColors);

    if(cs === 'l_right'){
      return(
        <div className={`object ${cs}`} style={{boxShadow: `0 10px 92px 0 #${shadowColor}`}}>
          <div className={`${cs}__mask`}>
            <div className={`${cs}__mask_bg`} style={{zIndex: 55}}>
              <svg height="100%" width="100%">
                <rect style={{mask: 'url(#background-horizontal-mask)'}}
                      className="doc__background-rectangle" height="100%" width="100%" fill={`#${itemLinkColors[0]}`} />
              </svg>
            </div>
          </div>

          <div className={`${cs}__media`}>
            <img onLoad={this.onLoadBasicImg}
                 className={`${cs}__img`}
                 src={`/${item.path}`}/>
          </div>

          <div className={`${cs}__layout_text`}>
            <div style={{color: 'white'}} className={`${cs}__layout_title`}>{item.title}</div>
            <div style={{color: 'white'}} className={`${cs}__layout_desc`}>{item.description}</div>
          </div>


        </div>
      )
    }

    if(cs === 'l_bottom'){
      return(
        <div className={`object ${cs}`} style={{boxShadow: `0 10px 92px 0 #${shadowColor}`}}>
          <div className={`${cs}__mask`}>
            <div className={`${cs}__mask_bg`} style={{zIndex: 55}}>
              <svg height="100%" width="100%">
                <rect style={{mask: 'url(#background-vertical-mask)'}}
                      className="doc__background-rectangle" height="100%" width="100%" fill={`#${itemLinkColors[0]}`} />
              </svg>
            </div>
          </div>

          <div className={`${cs}__media`}>
            <img onLoad={this.onLoadBasicImg}
                 className={`${cs}__img`}
                 src={`/${item.path}`}/>
          </div>

          <div className={`${cs}__layout_text`}>
            <div style={{color: 'white'}} className={`${cs}__layout_title`}>{item.title}</div>
            <div style={{color: 'white'}} className={`${cs}__layout_desc`}>{item.description}</div>
          </div>


        </div>
      )
    }

    if(cs === 'l_read_more'){
      return(
        <div className={`object ${cs}`} style={{boxShadow: `0 10px 92px 0 #${shadowColor}`}}>

          <div className={`${cs}__media`}>
            <img onLoad={this.onLoadBasicImg}
                 className={`${cs}__img`}
                 src={`/${item.path}`}/>
          </div>

          <div className={`${cs}__layout`} style={{zIndex: 50}}>
            <div className={`${cs}__r`}>
              <div className={`${cs}__r_text`}>
                Read more
              </div>
              <div className={`${cs}__r_arr`}>
                <div className={`${cs}__r_arr_icon`}>

                </div>
              </div>
            </div>
          </div>

          <div className={`${cs}__slide`} style={{zIndex: 60}}>

          </div>

        </div>
      )
    }

    return(
      <div className={`object ${cs}`} style={{boxShadow: `0 10px 92px 0 #${shadowColor}`}}>
        <div className={`${cs}__media`}>
          <img onLoad={this.onLoadBasicImg}
                    className={`${cs}__img`}
                    src={`/${item.path}`}/>
        </div>

        <div className={`${cs}__layout`}>
          <div className={`${cs}__layout_title`}>{item.title}</div>
        </div>


      </div>
    )
  };

  onChangeInputLink = (e) => {
    this.setState({itemLinkValue: e.target.value});
    RestApi({link: e.target.value}, '').then(response => {
      let item = response.data;
      item['path'] = item.path.split('\\').slice(-4).join('/');
      this.setState({itemLink: item});
      console.log('response', this.state.itemLink);
    }).catch(error => {
      console.log('error', error)
    });
  };

  setItemLinkColors = (colors) => {
    this.setState({itemLinkColors: colors})
  };

  setItemLayout = (e) => {
    if(!e)return;
    e.style.zIndex = this.props.appState.setItemLayout(e);
  };

  onActiveItem = (e, item) => {
    let { items } = this.state;
    this.setState({items: items.map(it=> {return {...it, is_active: it.id === item.id}})})
  };

  renderLink = (width) => {
    let { appState } = this.props;
    const { items, itemLink: item, linkOpen, itemItemLoading, drawMode, itemLinkColor: shadowColor } = this.state;

    if(item){
      return(
        <Item
          setItemLinkColors={this.setItemLinkColors}
          linkOpen={linkOpen}
          item={this.props.item}
          onLoadBasicImg={this.onLoadBasicImg}
        >
          <div ref={this.setItemLayout}
               className="layout" style={{ pointerEvents: this.state.drawOpen ? 'all' : 'none',
            zIndex: 100, position: 'absolute', width: '100%', height: '100%'}}>
            <DrawCanvas
              width={this.state.width}
              brushColor={this.state.brushColor || 'red'}
              brushSize={this.state.brushSize || 30}
              height={this.state.height}
              drawOpen={this.state.drawOpen}/>
          </div>

          {items.map((item, i)=>{
            return(
              <ElementItem key={i}
                           onClickTrash={this.onClickTrash}
                           setItemLayout={this.setItemLayout}
                           onActiveItem={this.onActiveItem}
                style={{zIndex: 100}} className="layout"
                           item={item}
                           image={item.item.path} top={100} left={100}
                           width={item.item.width} height={item.item.height}/>
            )
          })}

        </Item>
      )
    }

    return(
      <div className="object" style={{boxShadow: `0 10px 92px 0 #${shadowColor}`}}>
             test
     </div>
    )
  };

  onClickPreview = (e) => {
    this.setState({previewMode: !this.state.previewMode})
  };

  renderPreview = () => {
    let {itemLinkColors} = this.state;
    return(
      <div className='editor'>
        <div className='editor__bg' style={{background: `radial-gradient(ellipse at center, #${itemLinkColors[0]} 0%,#${itemLinkColors[1]} 47%,#000000 100%)`}} />
        <div className="editor__inner">
           {this.renderLink(1000)}
        </div>

        <div className='a_actions'>
            <div className='a_actions__inner'>
              <div className='a_actions__menu'>

                <div onClick={(e) => {this.setState({previewMode: false})}} >
                  <div className='a_actions__btn_preview'>
                    <div className='a_actions__btn_text'>
                     Back
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


      </div>
    )
  };

  onSelectColor = (e, color) => {
    //console.log('onSelectColor', e, color);
    this.setState({brushColor: color});
    setTimeout(()=>{
      this.renderPreviewCanvas();
    }, 100);
  };

  onSetBrushSize = (size) => {
    this.setState({brushSize: size})
  };

  renderDrawList = () => {
    return(
      <div className='objects__template'>
        <div className='objects__template_inner'>
          <div className='d_objects__colors'>
            <Palette
              onSelectColor={this.onSelectColor}
              brushColor={this.props.brushColor}/>
          </div>
          <div className='d_objects__prefs'>
            <SlideRange brushColor={this.state.brushColor}
                        brushSize={this.state.brushSize}
                        onSetBrushSize={this.onSetBrushSize} />
          </div>
          {/* <div className='d_objects__prefs'>
            <SlideRange brushColor={this.state.brushColor}
                        brushSize={this.state.brushSize}
                        opacity={true}
                        onSetBrushSize={this.onSetBrushSize} />
          </div>*/}
          {/*<div className='d_objects__canvas'>
            <div className='d_objects__canva_inner'>

              <canvas
                height="100%"
                className="d_objects__canva"
                id="d_objects__canva"
              />
            </div>
          </div>*/}
        </div>
      </div>
    )
  };

  renderPreviewCanvas = () => {
    return;
    let { appState } = this.props;
    const canvas = document.querySelector('.d_objects__canva');
    const s = document.querySelector('.objects');
    const ctx = canvas.getContext('2d');
    s.style.width = `${s.innerWidth}px`;
    canvas.style.height = `150px`;
    ctx.clearRect(0, 0, s.innerWidth, 150);
    let color = this.state.brushColor || 'red';
    /*appState.get_color(parseInt(this.state.brushTrans) === 100 ? '1' : `0.${this.state.brushTrans}`,
      null, this.state.brushColor);*/

    console.log('renderPreviewCanvas', color,
      this.state.brushColor, this.state.brushSize, this.state.brushTrans);

    ctx.lineWidth = this.state.brushSize || 20;
    ctx.lineCap = 'round';
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.moveTo(75,25);
   // ctx.quadraticCurveTo( 50, 100, 2s00, 30, 89 );
    ctx.quadraticCurveTo(50,120,30,125);
    // ctx.moveTo( 30, 200 );
    ctx.stroke();
    ctx.closePath();
  };


  onClickFinish = () => {
    let { items, itemLink } = this.state;

    let obj = document.querySelector('.object');
    let width = obj.offsetWidth;
    let height = obj.offsetHeight;

    items = items.map(it=>{
      let pos = it.pos;
      let node = pos['node'];
      //delete pos['node'];
      return {
        uuid: it.id.toString(),
        id: it.item.id,
        path: it.item.path,
        pos: {
          x: it.pos.x,
          y: it.pos.y,
          deltaX: it.pos.deltaX,
          deltaY: it.pos.deltaY,
          lastX: it.pos.lastX,
          lastY: it.pos.lastY,
        },
        width: node.offsetWidth,
        height: node.offsetHeight
      }
    });
    let data = {
      items,
      itemLink,
      width,
      height
    };

    RestApi(data, '').then(response => {
      console.log('response', response.data);
    }).catch(error => {
      console.log('error', error)
    });
    console.log('onClickFinish', data)
  };

  onClickTrash = () => {
    let { items } = this.state;
    console.log('onClickTrash', items.filter(t=>t.is_active));
    this.setState({items: items.filter(t=> !t.is_active)})
  };

  render() {
    const { objectsOpen, templatesOpen, layoutItem, itemLinkValue, itemLink,
      previewMode, drawOpen, itemLinkColors, items,
      elementsOpen, stickersOpen, emojiOpen } = this.state;

    if(previewMode){
      return(
        <div className='share'>
          {this.renderPreview()}
        </div>
      )
    }

    return (
      <div className='share'>
        <div className="share__bg"
             style={{background: `radial-gradient(ellipse at center,
             #${itemLinkColors[0]} 0%,#${itemLinkColors[1]} 47%,#000000 100%)`}}
        />

        {items.some(t=>t.is_active) ? <div onClick={this.onClickTrash} className='btn_trash'>
          <div className='btn_trash__icon'>
          </div>
        </div> : null}

        <div className='editor'>

          <Motion key={1} style={{x: spring(objectsOpen ? 0 : 165),
            y: spring(drawOpen ? 80 : 200)}}>
            {({x, y}) =>
              <div className='objects' style={{
                height: drawOpen ? `${y}px`: `${y}px`, /* drawOpen ? '80px': '200px',*/
                WebkitTransform: `translate3d(0, ${x}px, 0)`,
                transform: `translate3d(0, ${x}px, 0)`,
              }}>
                <div className='objects__header_m'>
                  <div className='objects__header_m_inner'>
                    <div className='objects__header_m_list'>

                      <div onClick={(e, it) => this.onClickObjects(e, 'linkOpen')}
                           className='objects__header_m_btn btn_effects'>

                      </div>

                      <div onClick={(e, it) => this.onClickObjects(e, 'templatesOpen')}
                           className='objects__header_m_btn btn_templates'>

                      </div>

                      <div onClick={(e, it) => this.onClickObjects(e, 'stickersOpen')}
                           className='objects__header_m_btn btn_sticker'>

                      </div>

                      <div onClick={(e, it) => this.onClickObjects(e, 'emojiOpen')}
                           className='objects__header_m_btn btn_emoji'>

                      </div>

                      <div onClick={(e, it) => this.onClickObjects(e, 'drawOpen')}
                           className='objects__header_m_btn btn_draw'>

                      </div>

                      <div onClick={(e, it) => this.onClickObjects(e, 'textOpen')}
                           className='objects__header_m_btn btn_text'>

                      </div>

                    </div>
                  </div>
                </div>
                <div className='objects__inner'>
                  {stickersOpen && this.renderStickersList()}
                  {emojiOpen && this.renderEmojisList()}
                  {elementsOpen && this.renderElementsList()}
                  {templatesOpen && this.renderTemplatesList()}
                  {drawOpen && this.renderDrawList()}
                </div>
              </div>

            }
          </Motion>

         <div className="editor__inner">
           {this.renderLink()}
         </div>
        </div>

      </div>
    );
  }
}
