import * as React from 'react';
import { observer, inject } from 'mobx-react';
import './style.scss';
import ColorPicker from './ColorPicker';
import { rgba } from '../../../services/utils';
import * as uuid from 'uuid';
import Rating from '../Item/Rating';
import Link from '../../Link';
import Heart from '../Item/Heart';
import Hashtag from '../Hashtag';
import Tabs from '../Header/HeaderMenu/Tabs';
import { Scrollbars } from 'react-custom-scrollbars';

// Minimum resizable area
var minWidth = 60;
var minHeight = 40;

// Thresholds
var FULLSCREEN_MARGINS = -10;
var MARGINS = 4;

// End of what's configurable.
var clicked = null;
var onRightEdge, onBottomEdge, onLeftEdge, onTopEdge;

var rightScreenEdge, bottomScreenEdge;

var preSnapped;

var b, x, y;

var redraw = false;
var ghostpane = document.getElementById('ghostpane');


let points = [];
const painters = [];
const mouseX = 0;
const mouseY = 0;
const stickersG = [{
  id: 1,
  uuid: null,
  path: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be2a.png',
  cat: 1,
  popularity: 100,
},
  {
    id: 2,
    uuid: null,
    path: 'http://www.stickpng.com/assets/thumbs/588a3cc6d06f6719692a2d0a.png',
    cat: 1,
    popularity: 100,
  },
  {
    id: 3,
    uuid: null,
    path: 'https://i.giphy.com/l3q2AuZRzjmzRie1G.gif',
    cat: 1,
    popularity: 101,
  },
  {
    id: 4,
    uuid: null,
    path: 'https://i.giphy.com/ToMjGpr2JbrKkbDotSE.gif',
    cat: 1,
    popularity: 101,
  },
  {
    id: 5,
    uuid: null,
    path: 'https://i.giphy.com/l0IyoV0i0YvzBdg64.gif',
    cat: 1,
    popularity: 101,
  },
];

const catStickers = [
  {
    id: 1,
    name: 'Recent',
    onlyIcon: true
  },
  {
    id: 2,
    name: 'Celebrities',
    haveGif: true,
    icon: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be5c.png'
  },
  {
    id: 3,
    name: 'gif',
    isGif: true,
    className: 'cat_st_gif',
  },
  {
    id: 4,
    name: 'At the Movies',
  },
  {
    id: 5,
    name: 'Comics and Fantasy',
  },
  {
    id: 6,
    name: 'Food',
  },
  {
    id: 7,
    name: 'Holidays',
  },
];

let emoji = [];

function midPointBtw(p1, p2) {
  return {
    x: p1.x + (p2.x - p1.x) / 2,
    y: p1.y + (p2.y - p1.y) / 2,
  };
}

let palette = [
  {'id': 1, 'color': 'E64646', is_active: true},
  {'id': 2, 'color': '00AEF9', is_active: false},
  {'id': 3, 'color': '62DA37', is_active: false},
  {'id': 4, 'color': 'CC74E1', is_active: false},
  {'id': 6, 'color': 'FFCB00', is_active: false},
  {'id': 7, 'color': '000000', is_active: false},
  {'id': 8, 'color': 'FFFFFF', is_active: false},
  {'id': 100, 'color': 'color', is_active: false},
];

@inject('appState')
@observer
export default class ShareItem extends React.Component {

  state = {
    canvas: null,
    context: null,
    drawing: false,
    currentCanvas: null,
    drawingText: false,
    editorMode: false,
    smileMode: false,
    activeEmojiItem: null,
    activeEmojiList: [],
    activeEmoji: false,
    activeEmojiDrop: true,
    activeEmojiDiv: null,
    emoji: [],
    emojiCat: [],
    lastX: 0,
    lastY: 0,
    lineWidth: 5,
    stickerMode: false,
    activeSticker: false,
    activeStickerItem: null,
    activeStickerDiv: null,
    activeStickerSize: false,
    activeStickerRotate: false,
    activeStickerDrop: false,
    activeStickers: [],
    activeCommentary: false,
    textComment: '',
    brushType: null,
    toggleText: false,
    activeTextDivTyping: false,
    activeTextDivActive: false,
    activeTextDivRotate: false,
    activeTextDrawing: false,
    activeTextDiv: null,
    inputText: '',
    textActive: false,
    drawMode: false,
    palette,
    canvasTexts: [],
    stickers: stickersG,
    catStickers,
    activeCatStickers: catStickers[0],
    selectedText: -1,
    mentionText: '',
    hashtagText: '',
    brushColor: '#E64646',
    brushSize: 20,
    brushTrans: 100,
    mouseX: 0,
    mouseY: 0,
    canvasLayerTexts: [],
    history: [],
    undoList: [],
    redoList: [],
    scrollLeft: false,
    scrollRight: true,
  };

  isMobile = () => {
    return false
  };


  componentDidMount() {
    const canvas = document.querySelector('.canvas');
    const { item, appState } = this.props;

    const { width, height } = item.arcImage(appState.isMobile() ? window.innerWidth : 600, 1300);

    canvas.width = width;
    canvas.height = height;

    canvas.parentNode.style = `width: ${width}px; height: ${height}px;`;
    canvas.parentNode.style.backgroundImage = `url(${item.img})`;

    const ctx = canvas.getContext('2d');

    ctx.lineWidth = 10;
    ctx.lineJoin = ctx.lineCap = 'round';

    this.setState({
      canvas,
      context: ctx,
    });


    const image = new window.Image();
    image.width = width;
    image.crossOrigin = '';
    image.height = height;
    image.src = item.img;
    /*image.onload = function () {
     ctx.drawImage(image, 0, 0, width, height);
     };*/

    window.addEventListener('mousemove', this.handleOnMouseMoveBlock);
    window.addEventListener('touchmove', this.handleOnMouseMoveBlock);

    window.addEventListener('mousedown', this.handleOnMouseDownBlock);
    window.addEventListener('touchdown', this.handleOnMouseDownBlock);

    window.addEventListener('mouseup', this.handleOnMouseUpBlock);
    window.addEventListener('touchend', this.handleOnMouseUpBlock);
    this.activeXPos = 0;
    this.activeYPos = 0;
    // console.log('requestAnimFrame', window.requestAnimFrame);
    // this.animateDrawGif();

    ghostpane = document.getElementById('ghostpane');
  }


  componentWillUnmount() {
    window.removeEventListener('mousemove', this.handleOnMouseMoveBlock);
    window.removeEventListener('touchmove', this.handleOnMouseMoveBlock);

    window.removeEventListener('mousedown', this.handleOnMouseDownBlock);
    window.removeEventListener('touchdown', this.handleOnMouseDownBlock);

    window.removeEventListener('mouseup', this.handleOnMouseUpBlock);
    window.removeEventListener('touchend', this.handleOnMouseUpBlock);
  }

  handleOnMouseDownBlock = (e) => {
    const { activeStickerDiv,
      activeTextDivTyping, activeStickerDrop } = this.state;

    /* if(cTarget.classList.contains('cr_text__selection_handler')){
     c
     }



     this.activeStickerDivX = this.activeXPos - cTarget.offsetLeft;
     this.activeStickerDivY = this.activeYPos - cTarget.offsetTop;*/
    this.activeXPos = e.clientX;
    this.activeYPos = e.clientY;

    let cTarget = e.target;
    let item = null;
    if(e.target.classList.contains('cr_sticker')){
      this.activeStickerDivX = this.activeXPos - cTarget.offsetLeft;
      this.activeStickerDivY = this.activeYPos - cTarget.offsetTop;
      item = this.state.activeStickers[0];

      this.setState({ activeStickerItem: item,
        activeStickerDrop: true,
        activeStickerDiv: cTarget });
    }
    this.activeStickerDivX = this.activeXPos - activeStickerDiv.offsetLeft;
    this.activeStickerDivY = this.activeYPos - activeStickerDiv.offsetTop;




    console.log('handleOnMouseUp7Block', this.state.activeStickerDrop);


    onTopEdge = false;
    onLeftEdge = false;
    onRightEdge = false;
    onBottomEdge = false;

    if(e.target.id === 'pe_nw'){
      onTopEdge = true;
      onLeftEdge = true;
    }
    else if(e.target.id === "pe_ne"){
      onTopEdge = true;
      onRightEdge = true;
      this.activeStickerDivX = this.activeStickerDivX - activeStickerDiv.offsetWidth/2;
    }
    else if(e.target.id === "pe_se"){
      onBottomEdge = true;
      onRightEdge = true;
      this.activeStickerDivX = this.activeStickerDivX - activeStickerDiv.offsetWidth/2;
      this.activeStickerDivY = this.activeStickerDivY - activeStickerDiv.offsetHeight/2;

    }
    else if(e.target.id === "pe_sw"){
      onBottomEdge = true;
      onLeftEdge = true;
      //this.activeStickerDivY = this.activeYPos - activeStickerDiv.offsetTop;
      // console.log('handleOnMouseDownBlock', e.target, isResizing, b,  activeStickerDiv.offsetHeight);
      this.activeStickerDivY = this.activeStickerDivY - activeStickerDiv.offsetHeight/2;
    }
    if(!activeStickerDiv)return;
    b = activeStickerDiv.getBoundingClientRect();

    let isResizing = onRightEdge || onBottomEdge || onTopEdge || onLeftEdge;

    if(isResizing){
      this.setState({activeStickerSize: true});
    }
    else{
      this.setState({activeStickerDrop: true});
    }

    clicked = {
      x: x,
      y: y,
      cx: this.activeXPos - this.activeStickerDivX,
      cy:this.activeYPos - this.activeStickerDivY,
      w: b.width,
      h: b.height,
      isResizing: isResizing,
      isMoving: !isResizing && this.canMove(),
      onTopEdge: onTopEdge,
      onLeftEdge: onLeftEdge,
      onRightEdge: onRightEdge,
      onBottomEdge: onBottomEdge
    };

    if (activeTextDivTyping && !e.target.classList.contains('cr_text__textarea')) {
      this.setState({ activeTextDivTyping: false, activeTextDivActive: false });
    }
    if (e.target.classList.contains('cr_text__selection_handler')) {
      this.setState({ activeStickerSize: true, activeStickerRotate: false });
    }
    if (e.target.classList.contains('cr_text__selection_rotate')) {
      this.setState({ activeStickerRotate: true, activeStickerSize: false });
    }
    //this.calc(e);
  };

  canMove = () => {
    return x > 0 && x < b.width && y > 0 && y < b.height
      && y < 30;
  };

  calc = (e) => {
    let { activeStickerDiv } = this.state;
    if(!activeStickerDiv)return;
    // let pane = activeStickerDiv;
    // b = pane.getBoundingClientRect();

    this.activeXPos = e.clientX;
    this.activeYPos = e.clientY;

    x = this.activeXPos - this.activeStickerDivX;
    y = this.activeYPos - this.activeStickerDivY;

    //  x = e.clientX - b.left;
    //  y = e.clientY - b.top;
    //console.log('ghg656gh', e.target);
    /*
     *        {<div className="cr_text__selection_handler" id="pe_nw" />}
     {<div className="cr_text__selection_handler" id="pe_ne" />}

     <div className='cr_text__selection_handler' id='pe_se' />
     {/*<div className='cr_text__selection_rotate' id='pe_rt' />}
     {<div className="cr_text__selection_handler" id="pe_sw" />}
     * */


    rightScreenEdge = window.innerWidth - MARGINS;
    bottomScreenEdge = window.innerHeight - MARGINS;
  };

  handleOnMouseUpBlock = (e) => {
    //this.calc(e);
    let { activeStickerDiv } = this.state;

    console.log('handleOnMouseUpBlock');
    this.activeStickerDivX = this.activeXPos - activeStickerDiv.offsetLeft;
    this.activeStickerDivY = this.activeYPos - activeStickerDiv.offsetTop;

    this.setState({ activeStickerDrop: false,
      activeStickerSize: false,
      activeStickerRotate: false,
      activeEmojiDrop: false,
      activeEmojiSize: false,
      activeEmojiRotate: false});

  };

  dragAndDrop = (e) => {
    let { activeStickerDiv } = this.state;
    if(!activeStickerDiv)return;
    let pane = activeStickerDiv;

    x = this.activeXPos - this.activeStickerDivX;
    y = this.activeYPos - this.activeStickerDivY;
    let w = 0;
    let h = 0;

    if (clicked && clicked.isResizing) {
      w = null;
      h = null;

      if (clicked.onRightEdge){
        w = Math.max(x, minWidth) + 'px';
        pane.style.width = w;
      }
      if (clicked.onBottomEdge) {
        h = Math.max(y, minHeight) + 'px';
        pane.style.height = h;
      }

      if (clicked.onLeftEdge) {
        var currentWidth = Math.max(clicked.cx - x + clicked.w, minWidth);
        if (currentWidth > minWidth) {
          pane.style.width = currentWidth + 'px';
          w = currentWidth + 'px';
          pane.style.left = x + 'px';
        }
      }

      if (clicked.onTopEdge) {
        var currentHeight = Math.max(clicked.cy - y  + clicked.h, minHeight);
        if (currentHeight > minHeight) {
          pane.style.height = currentHeight + 'px';
          h = currentHeight + 'px';
          pane.style.top = y + 'px';
        }
      }

      if(w){
        activeStickerDiv.firstChild.firstChild.style.width = w;
      }
      if(h){
        activeStickerDiv.firstChild.firstChild.style.height = h;
      }


      //  hintHide();

      return;
    }

  };

  setBounds = (element, x, y, w, h) => {
    element.style.left = x + 'px';
    element.style.top = y + 'px';
    element.style.width = w + 'px';
    element.style.height = h + 'px';
  };

  handleOnMouseMoveBlock = (e) => {
    const { activeTextDiv, activeTextDivTyping,
      activeTextDrawing, activeStickerItem,
      activeStickerDiv,
      activeStickerDrop,
      activeStickerSize,
      activeEmojiItem,
      activeEmojiDiv,
      activeEmojiDrop,
      activeEmojiSize,
      activeEmojiRotate,
      activeStickerRotate,
      activeTextDivRotate } = this.state;

    this.calc(e);
    //console.log('handleOnMouseMoveBlock', activeStickerItem
    //  , activeStickerDiv, activeStickerDrop);
    if (activeStickerItem
      && activeStickerDiv && activeStickerSize) {
      this.dragAndDrop(e);
    }
    if (activeStickerItem
      && activeStickerDiv && activeStickerDrop) {
      x = this.activeXPos - this.activeStickerDivX;
      y = this.activeYPos - this.activeStickerDivY;
      activeStickerDiv.style.top = `${y}px`;
      activeStickerDiv.style.left = `${x}px`;
    }
    return;
    let x = 0,
      y = 0;
    this.activeXPos = e.clientX;
    this.activeYPos = e.clientY;
    if (activeStickerItem
      && activeStickerDiv && activeStickerDrop) {
      x = this.activeXPos - this.activeStickerDivX;
      y = this.activeYPos - this.activeStickerDivY;


      if (activeStickerSize) {
        const w = Math.max(this.activeXPos - activeStickerDiv.getBoundingClientRect().left, 30);
        let ratio = Math.min(w / activeStickerDiv.offsetWidth);
        let h = activeStickerDiv.offsetHeight*ratio;
        activeStickerDiv.style.width = `${w}px`;
        activeStickerDiv.style.height = `${h}px`;
        activeStickerDiv.firstChild.firstChild.style.width = `${w}px`;
        activeStickerDiv.firstChild.firstChild.style.height = `${w}px`;

        return;
      }
      if (activeStickerRotate) {
        const center_x = (activeStickerDiv.getBoundingClientRect().left) + (activeStickerDiv.offsetHeight / 2);
        const center_y = (activeStickerDiv.getBoundingClientRect().top) + (activeStickerDiv.offsetWidth / 2);
        const radians = Math.atan2(x - center_x, y - center_y);
        const degree = (radians * (180 / Math.PI) * -1) + 90;
        // console.log('activeStickerRotate', radians, degree);
        activeStickerDiv.style.transform = `rotateZ(${degree}deg)`;
        return;
      }

      activeStickerDiv.style.top = `${y}px`;
      activeStickerDiv.style.left = `${x}px`;
    }

    if (activeEmojiItem
      && activeEmojiDiv && activeEmojiDrop) {
      x = this.activeXPos - this.activeEmojiDivX;
      y = this.activeYPos - this.activeEmojiDivY;


      if (activeEmojiSize) {
        const w = Math.max(this.activeXPos - activeEmojiDiv.getBoundingClientRect().left, 30);
        activeEmojiDiv.style.width = `${w}px`;
        activeEmojiDiv.style.height = `${w}px`;
        activeEmojiDiv.firstChild.firstChild.style.width = `${w}px`;
        activeEmojiDiv.firstChild.firstChild.style.height = `${w}px`;

        return;
      }
      if (activeEmojiRotate) {
        const center_x = (activeEmojiDiv.getBoundingClientRect().left) + (activeEmojiDiv.offsetHeight / 2);
        const center_y = (activeEmojiDiv.getBoundingClientRect().top) + (activeEmojiDiv.offsetWidth / 2);
        const radians = Math.atan2(x - center_x, y - center_y);
        const degree = (radians * (180 / Math.PI) * -1) + 90;
        // console.log('activeStickerRotate', radians, degree);
        activeEmojiDiv.style.transform = `rotateZ(${degree}deg)`;
        return;
      }

      activeEmojiDiv.style.top = `${y}px`;
      activeEmojiDiv.style.left = `${x}px`;
    }

  };

  handleOnMouseDownSticker = (e, item) => {
    const cTarget = e.currentTarget;

    this.activeStickerDivX = this.activeXPos - cTarget.offsetLeft;
    this.activeStickerDivY = this.activeYPos - cTarget.offsetTop;

    this.setState({ activeStickerItem: item,
      activeStickerDrop: true,
      activeStickerDiv: cTarget });
  };

  handleOnMouseDownEmoji = (e, item) => {
    const cTarget = e.currentTarget;

    this.activeEmojiDivX = this.activeXPos - cTarget.offsetLeft;
    this.activeEmojiDivY = this.activeYPos - cTarget.offsetTop;

    this.setState({ activeEmojiItem: item,
      activeEmojiDrop: true,
      activeEmojiDiv: cTarget });
  };

  handleOnMouseUpSticker = (e, item) => {
    this.setState({ activeStickerDrop: false,
      activeStickerSize: false,
      activeStickerRotate: false });
  };

  onInputComment = (e) => {
    //console.log('onInputComment', e.target.value);
    this.setState({textComment: e.target.value})
  };

  renderComment = (text) => {
    let matched = null;
    let texts = [];
    text = text.replace(new RegExp('#', 'g'), ' #');
    text.split(' ').forEach(txt=>{
      matched = txt.match(/#([^\\s]*)/gi);
      matched && [].forEach.call(matched, function(m) {
        txt = txt.replace(m, '');
        texts.push(txt);
        texts.push(<Hashtag value={m}/>);
        texts.push(' ');
      });
      !matched && texts.push(`${txt} `);
    });
    return texts
  };

  renderItemContent = (item) => {
    let {activeCommentary, textComment} = this.state;
    return (
      <div className='fit-content'>
        <div className='fit-item-image-container card__image'>

          <div className='card__overlay card__overlay--indigo'>
            <div className='card__content'>
              <div className='card__content__block'>
                {/* <div className="card__content__rating">
                 <Rating item={item} />
                 </div>*/}
                <div className='card__content__title'>
                  <div className='card__content__title__link'>
                    <a target='_black' href={item.url} className='card__title'>{item.title}</a>
                  </div>

                  <div className='card__content__title__comment'>
                    {activeCommentary ?
                      <div className="share_comment">
                          <textarea onInput={this.onInputComment}
                                    className="share_comment__text" value={textComment}/>
                      </div>
                      : this.renderComment(textComment)}
                  </div>

                  <div className='card__content__title__source'>

                    <div className='card__content__title__source_top'>
                      <div>
                        {item.wallType !== 5 ? <Link
                          type='profile' style={{ textDecoration: 'none' }}
                          href={`/${item.site.slug}`} value={item.site.short_url} to={`/${item.site.slug}`} {...this.props}
                        /> : null}
                        {item.wallType === 5 ?
                          <div>
                            <a title={item.user.first_name} href={`/${item.user.username}`}>@{item.user.username}</a>
                            {item.site.short_url ? ' from ' : ''}
                            {item.site.short_url ?
                              <Link
                                type='profile' style={{ textDecoration: 'none' }}
                                href={`/${item.site.slug}`} value={item.site.short_url} to={`/${item.site.slug}`} {...this.props}
                              />
                              : null}
                          </div>
                          : null}
                      </div>
                      <div className='card__content__time'>
                        {item.fromNow() }
                      </div>
                      {/*<div className='card__content__rating'>
                       <Rating Rating={this.Rating} item={item} />
                       </div>*/}
                    </div>

                    <div className='card__content__title__source_bottom'>
                      <div className='card__content__button'>
                        <div onClick={(it) => { return this.Share(item); }} className='card__content__share'>
                          Share +15
                        </div>

                        {/*<Heart Like={this.Like} item={item} {...this.props} />*/}

                        <div className='card__content__more'>
                          <div className='card__content__more_inner'>
                            <div />
                            <div />
                            <div />
                          </div>
                        </div>

                      </div>
                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>);
  };


  setStickerMode = () => {
    let editorMode = true;
    if(!this.state.smileMode && this.state.stickerMode){
      editorMode = false;
    }
    this.setState({
      brushType: null,
      textActive: false,
      drawMode: false,
      activeEmojiItem: null,
      activeStickerDiv: false,
      activeStickerItem: null,
      stickerMode: !this.state.stickerMode,
      smileMode: false,
      editorMode: editorMode,
      activeTextDivRotate: false,
      activeTextDivActive: false,
      activeTextDiv: false,
      activeText: false,
    });
  };

  setSmileMode = () => {
    let editorMode = true;
    if(this.state.smileMode && !this.state.stickerMode){
      editorMode = false;
    }
    this.setState({
      brushType: null,
      textActive: false,
      drawMode: false,
      activeStickerDiv: false,
      activeStickerItem: null,
      activeEmojiItem: null,
      stickerMode: false,
      smileMode: !this.state.smileMode,
      editorMode: editorMode,
      activeTextDivRotate: false,
      activeTextDivActive: false,
      activeTextDiv: false,
      activeText: false,
    });
    // TODO in indexdb
    if(!this.state.emoji.length){
      this.loadEmoji()
    }
  };

  loadEmoji = () => {
    // eslint-disable-next-line global-require, import/no-dynamic-require
    let res = require(`../../../emoji.json`);
    console.log('loadEmoji', res);
    let categ = [],
      cat = null,
      categories = [],
      emoji = [],
      i =  1;
    categories.push({id: 0, name: 'recent', active: true,
      recentIcon: true, onlyIcon: true, emoji: []});
    for(let key in res){
      if(categ.indexOf(res[key].category) === -1){
        categ.push(res[key].category);
        categories.push({id: i, name: res[key].category, active: false, emoji: []});
        i++;
      }
      emoji.push(res)
    }
    for(let key in res){
      cat = categories.filter(it=>it.name === res[key].category)[0];
      cat.emoji.push(res[key]);
      //console.log('loadEm3oqji', cat, res[key].category)
    }
    this.setState({emojiCat: categories, emoji: emoji});
    //console.log('loadEmoqji', categories, emoji);
  };

  onClickSticker = (item) => {
    const { activeStickers } = this.state;
    const activeStickersT = activeStickers;
    item.uuid = uuid.v4();
    activeStickersT.push(item);
    this.setState({ activeStickers,
      activeStickersT,
      activeStickerItem: null,
      activeSticker: true,
    });
    console.log('fgfg56fg', this);
  };

  onClickTabEmojiCat = (i) => {
    let { emojiCat } = this.state;
    let emojiCats = emojiCat.filter(it=>it.name !== 'modifier')
      .map((item, z)=>{
        item.active = i === z;
        return item
      });
    this.setState({emojiCat: emojiCats});
    console.log('onClickTabEmojiCat', i)
  };

  onScrollEmojiCat = (e) => {
    let diff = e.currentTarget.scrollWidth - e.currentTarget.scrollLeft;
    //console.log('onScrollEmojiCat', diff)
  };

  onClickEmoji = (item) => {
    console.log('onClickEmoji', item);
    let { activeEmojiList } = this.state;
    const activeEmojiT = activeEmojiList;
    item.uuid = uuid.v4();
    activeEmojiT.push(item);
    this.setState({ activeEmojiList: activeEmojiT,
      activeEmojiItem: item,
      activeEmoji: true,
    });
  };

  onClickTrash = (item) => {
    let { activeEmojiList, activeStickers } = this.state;
    let activeEmojiListT = activeEmojiList;
    let activeStickersT = activeStickers;
    console.log('onClickTrash', item);
    if((item||{}).unicode){
      activeEmojiListT = activeEmojiListT.map((v,i) => v.uuid !== item.uuid ? v : -1).filter(v => v !== -1);
    }
    else{
      activeStickersT = activeStickersT.map((v,i) => v.uuid !== item.uuid ? v : -1).filter(v => v !== -1);
    }
    this.setState({
      activeStickerItem: null,
      activeStickers: activeStickersT,
      activeEmojiList: activeEmojiListT,
      activeEmojiItem: null})
  };

  onClickComment = () => {
    console.log('onClickComment');
    this.setState({
      activeCommentary: !this.state.activeCommentary
    })
  };

  handleOnMouseDown = (e) => {
    let rect = this.state.canvas.getBoundingClientRect();

    if(this.isMobile()){
      this.setState({
        lastX: e.targetTouches[0].pageX - rect.left,
        lastY: e.targetTouches[0].pageY - rect.top
      });
    }
    else{
      this.setState({
        lastX: e.clientX - rect.left,
        lastY: e.clientY - rect.top,
        mouseX: e.clientX - rect.left,
        mouseY: e.clientY - rect.top
      });
    }

    if(!this.state.brushType)return;

    this.setState({
      drawing: true,
      drawingText: true
    });
  };

  handleOnMouseMove = (e) => {
    let { appState } = this.props;
    let rect = this.state.canvas.getBoundingClientRect();
    let ctx = this.state.context;

    if(this.state.drawing) {

      if (this.state.brushType === 1) {
        points.push({
          x: e.clientX - rect.left,
          y: e.clientY - rect.top
        });
        let color = appState.get_color(parseInt(this.state.brushTrans) === 100 ? '1' : `0.${this.state.brushTrans}`,
          null, this.state.brushColor);
        // ctx.shadowBlur = 0;
        ctx.lineWidth = this.state.brushSize;
        ctx.lineCap = 'round';
        ctx.strokeStyle = color;

        var p1 = points[0];
        var p2 = points[1];

        ctx.beginPath();
        ctx.moveTo(p1.x, p1.y);

        for (var i = 1, len = points.length; i < len; i++) {
          var midPoint = midPointBtw(p1, p2);
          ctx.lineWidth = this.state.brushSize;
          ctx.lineCap = 'round';
          ctx.strokeStyle = color;
          ctx.quadraticCurveTo(p1.x, p1.y, midPoint.x, midPoint.y);
          p1 = points[i];
          p2 = points[i + 1];
        }
        ctx.lineTo(p1.x, p1.y);
        ctx.stroke();
      }
    }
  };

  handleOnMouseUp = (e) => {
    points = [];
    this.setState({
      drawing: false,
      drawingText: false
    });
  };

  setBrush = (brushType) => {
    console.log('setBrush', brushType);
    if(brushType === this.state.brushType)brushType = null;
    this.setState({brushType: brushType, textActive: false,
      drawMode: brushType !== null,
      activeTextDivRotate: false,
      activeTextDivActive: false,
      editorMode: true,
      activeStickerItem: null,
      activeEmojiItem: null,
      stickerMode: false,
      smileMode: false,
      activeTextDiv: false,
      activeText: false});

    setTimeout(()=>{
      this.renderPreviewCanvas();
    }, 100);

  };

  onClickPalette = (e, item) => {
    let { palette } = this.state;
    this.setState({brushColor: `#${item.color}`, palette: palette.map(it=>{
      it.is_active = it.id === item.id;
      return it
    })});
    setTimeout(()=>{
      this.renderPreviewCanvas();
    }, 200);
    console.log('onClickPalette', e, item);
  };

  onChangeTrans = (e) => {
    console.log('onChangeTrans', e.target.value);
    this.setState({brushTrans: e.target.value});
    setTimeout(()=>{
      this.renderPreviewCanvas();
    }, 200);
  };

  onChangeBrushSize = (e) => {
    console.log('onChangeTrans', e.target.value);
    this.setState({brushSize: e.target.value});
    setTimeout(()=>{
      this.renderPreviewCanvas();
    }, 200);
  };

  renderPreviewCanvas = () => {
    let { appState } = this.props;
    const canvas = document.querySelector('.draw_block__canvas');
    const ctx = canvas.getContext('2d');
    let width = this.state.context.canvas.width;
    let height = this.state.context.canvas.height;
    ctx.clearRect(0, 0, width, height);
    let color = appState.get_color(parseInt(this.state.brushTrans) === 100 ? '1' : `0.${this.state.brushTrans}`,
      null, this.state.brushColor);

    console.log('renderPreviewCanvas', color,
      this.state.brushColor, this.state.brushSize, this.state.brushTrans);

    ctx.lineWidth = this.state.brushSize;
    ctx.lineCap = 'round';
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.moveTo( 30, 30 );
    ctx.quadraticCurveTo( 50, 100, 200, 30 );
    ctx.stroke();
    ctx.closePath();
  };

  onClickStickerInfo = (e, item) => {
    console.log('onClickStickerInfo', e, item);
    e.preventDefault()
  };

  onClickScroll = (e, direction) => {
    let { scrollLeft: scrollLeftT, scrollRight: scrollRightT }= this.state;
    let scrollLeft = e.currentTarget.parentNode.firstChild.firstChild.scrollLeft;
    let width = e.currentTarget.parentNode.offsetWidth;
    let scrollWidth = e.currentTarget.parentNode.firstChild.firstChild.scrollWidth;
    let summa =  scrollLeft + width;
    if(direction === 'left'){
      summa = scrollLeft - width;
    }
    e.currentTarget.parentNode.firstChild.firstChild.scrollLeft = summa;

    let scrollLeftNew = e.currentTarget.parentNode.firstChild.firstChild.scrollLeft;
    console.log('onClickScroll', scrollLeftNew, scrollLeft, scrollWidth, direction);

    if(direction === 'right' && !scrollLeftT){
      this.setState({scrollLeft: true})
    }

  };

  render() {
    const { item, user, appState, styleImg, widthBlock, grid, heightBlock } = this.props;
    const { toggleText, drawing, activeTextDivTyping,
      stickerMode, stickers, activeStickers,
      emojiCat, emoji, activeEmojiItem, activeEmoji,
      activeEmojiList, palette, brushTrans, brushSize,
      editorMode, catStickers, activeCatStickers,
      activeStickerItem, activeSticker, smileMode,
      scrollRight, scrollLeft,
      drawMode, activeText, activeTextDiv, activeTextDivActive,
      inputText, canvasLayerTexts } = this.state;
    // console.log('Item2', item.img);
    let activeEmojiCat = emojiCat.some(it=> it.active) ? emojiCat.filter(it=> it.active)[0] : null;

    const { width, height } = item.arcImage(appState.isMobile() ? window.innerWidth : 600, 300);
    const styleImgBase = styleImg;
    const styleItem = styleImg || { width: `${width}px`, height: `${height}px` };
    // if(!grid){styleItem['backgroundImage'] = `url(${item.img})`;}
    if (heightBlock) { styleImg.height = heightBlock; }
    if (widthBlock) { styleImg.width = widthBlock; }
    if (grid) { delete styleItem.top; delete styleItem.left; }
    let color = appState.color;

    ///console.log('activeEmojiCat', activeEmojiCat);


    return (
      <div className='share__block'>
        <div className='share__block__top'>
          <div className='share_top'>
            <div>left</div>

            <div className='share_top__center_menu'>
              {activeEmojiItem || activeStickerItem ?
                <div
                  onClick={(e, tr) => this.onClickTrash(activeEmojiItem || activeStickerItem)}
                  className='share_top__center_menu_item share_top__trash' />
                : null}
            </div>

            <div className='share_top__menu'>
              <div onClick={this.onClickComment} className='share_top__menu_item share_top__comment' />
              <div onClick={(tp) => this.setBrush(1)} className='share_top__menu_item share_top__brush b_type_3' />
              <div onClick={(tp) => { return this.setStickerMode(); }} className='share_top__menu_item share_top__sticker' />
              <div onClick={(tp) => { return this.setSmileMode(); }} className='share_top__menu_item share_top__smile' />
            </div>
          </div>
        </div>

        <div className='share__block__center'>
          <div/>
          <div style={styleImg} className='share_item'>
            <div className='canvas_block' style={{overflow: editorMode ? 'visible' : 'hidden'}}>
              <div className='item__card card' >
                <canvas
                  style={{ pointerEvents: drawMode ? 'all' : 'none' }}
                  className='canvas'
                  id='canvas'
                  width={width}
                  height={height}
                  onMouseDown = {this.handleOnMouseDown}
                  onTouchStart = {this.handleOnMouseDown}
                  onMouseMove = {this.handleOnMouseMove}
                  onTouchMove = {this.handleOnMouseMove}
                  onMouseUp = {this.handleOnMouseUp}
                  onTouchEnd = {this.handleOnMouseUp}
                />
                {!editorMode ? this.renderItemContent(item) : null}
              </div>

              <div
                style={{ pointerEvents: !drawMode ? 'all' : 'none' }}
                className='layer__emoji'
              >
                {activeEmojiList.map((item, i) => {
                  return (
                    <div
                      key={item.uuid}
                      // onClick={(it) => this.onClickStickerView(item)}
                      onMouseDown={(e, it) => { return this.handleOnMouseDownEmoji(e, item); }}
                      onTouchStart={(e, it) => { return this.handleOnMouseDownEmoji(e, item); }}
                      // onMouseUp = {(e, it) => this.handleOnMouseUpSticker(e, item)}
                      // onTouchEnd = {(e, it) => this.handleOnMouseUpSticker(e, item)}
                      className={`cr_emoji ${(activeEmojiItem === item ? ' active ' : null)}`}
                    >
                      <div className='cr_emoji__inner'>
                        <img className='cr_emoji__img' src={`/assets/emoji/png_128x128/${item.unicode}.png`} />
                      </div>
                      {activeEmojiItem === item ?
                        <div className='cr_text__selection'>
                          {/* <div className="cr_text__selection_handler" id="pe_nw" />*/}
                          {/* <div className="cr_text__selection_handler" id="pe_ne" />*/}

                          <div className='cr_text__selection_handler' id='pe_se' />
                          <div className='cr_text__selection_rotate' id='pe_rt' />
                        </div>
                        : null}
                    </div>
                  );
                })}
              </div>

              <div
                style={{ pointerEvents: !drawMode ? 'all' : 'none' }}
                className='layer__sticker'
              >
                {activeStickers.map((item, i) => {
                  return (
                    <div
                      key={item.uuid}
                      // onClick={(it) => this.onClickStickerView(item)}
                      // onMouseDown={(e, it) => { return this.handleOnMouseDownSticker(e, item); }}
                      //onTouchStart={(e, it) => { return this.handleOnMouseDownSticker(e, item); }}
                      // onMouseUp = {(e, it) => this.handleOnMouseUpSticker(e, item)}
                      // onTouchEnd = {(e, it) => this.handleOnMouseUpSticker(e, item)}
                      className={`cr_sticker ${(activeStickerItem === item ? ' active ' : '')}`}
                    >
                      <div className='cr_sticker__inner'>
                        <img className='cr_sticker__img' src={item.path} />
                      </div>
                      {activeStickerItem === item ?
                        <div className='cr_text__selection'>
                          {<div className="cr_text__selection_handler" id="pe_nw" />}
                          {<div className="cr_text__selection_handler" id="pe_ne" />}

                          <div className='cr_text__selection_handler' id='pe_se' />
                          {/*<div className='cr_text__selection_rotate' id='pe_rt' />*/}
                          {<div className="cr_text__selection_handler" id="pe_sw" />}
                        </div>
                        : null}
                    </div>
                  );
                })}
              </div>

            </div>
          </div>
          <div></div>
        </div>



        <div className='share__block__bottom'>

          {!editorMode ? <div className='share_btn'>
            <div style={{backgroundColor: color}} className='share_btn__inner'>
              <div className='share_btn__title'>SHARE</div>
            </div>
          </div> : null}

          {drawMode ?
            <div className='draw_block'>
              <div className='draw_block__inner'>
                <div className='draw_block__item draw_block__palette'>
                  {palette.filter(item=>item.color !== 'color').map(item=> {
                    return(
                      <div
                        onClick={(e, it) => this.onClickPalette(e, item)}
                        style={{backgroundColor: `#${item.color}`}}
                        className={`draw_block__palette_item ${(item.is_active ? ' active ': '')}
                        ${(item.color === 'FFFFFF' ? ' white ': '')}
                        ${(item.color === 'color' ? ' colour ': '')}`}>

                      </div>
                    )
                  })}
                </div>
                <div className='draw_block__item'>
                  <div className='draw_block__item_sl'>
                    <div className='draw_block__item_sl_tr'>
                      <div className='draw_block__item_sl_label'>
                        Intensity <span>{brushTrans}%</span>
                      </div>
                      <input onChange={this.onChangeTrans} className='draw_block__item_sl_tr_range'
                             type="range" min="0" max="100" step="1" value={brushTrans} />
                    </div>
                    <div className='draw_block__item_sl_wh'>
                      <div className='draw_block__item_sl_label'>
                        Line Thickness <span>{brushSize}</span>
                      </div>
                      <input onChange={this.onChangeBrushSize} className='draw_block__item_sl_tr_range'
                             type="range" min="1" max="50" step="1" value={brushSize} />
                    </div>
                  </div>
                </div>
                <div className='draw_block__item'>
                  <canvas
                    height="100%"
                    className="draw_block__canvas"
                    id="draw_block__canvas"
                  />
                </div>
              </div>
            </div>
            : null}


          {smileMode ?
            <div className='emoji_block'>
              <div className='emoji_block__menu' >
                <Tabs
                  tabsClassName="emoji_tab"
                  classnam='sticker_block__menu_item'
                  onTabChange={this.onClickTabEmojiCat} selected={0}
                  showIcon={true}
                  tabs={emojiCat.filter(it=>it.name !== 'modifier').map(item => {
                    return {
                      icons: item.emoji.map(item => {
                        return `/assets/emoji/png/${item.unicode}.png`
                      }),
                      onlyIcon: item.onlyIcon,
                      icon: item.icon,
                      displayName: item.name,
                      key: item.id
                    };
                  })} />
              </div>
              <div className='emoji_block__items'>
                <Scrollbars onScroll={this.onScrollEmojiCat}
                            className='emoji_block__scroll'
                            style={{ width: '100%', height: '60px'}}>
                  {activeEmojiCat && activeEmojiCat.emoji.map(item => {
                    return (
                      <div onClick={(it) => { return this.onClickEmoji(item); }}
                           key={item.unicode} className='emoji_block__emoji'>
                        <img className='emoji_block__emoji_img' src={`/assets/emoji/png/${item.unicode}.png`} />
                      </div>
                    );
                  })}
                </Scrollbars>

                {scrollLeft ? <div
                  onClick={(e, tp) => this.onClickScroll(e, 'left')}
                  className="sb_scroll__item sb_scroll__left">
                  <div className="sb_scroll__block" ><div/></div>
                </div> : null}

                {scrollRight ? <div
                  onClick={(e, tp) => this.onClickScroll(e, 'right')}
                  className="sb_scroll__item sb_scroll__right">
                  <div className="sb_scroll__block" ><div/></div>
                </div> : null}

              </div>
            </div>
            : null}

          {stickerMode ?
            <div className='sticker_block'>
              <div className='sticker_block__menu'>
                {/*<div className='sticker_block__menu_recent sticker_block__menu_item'>
                 <div />
                 </div>*/}
                <Tabs tabsClassName="sticker_tab" classname='sticker_block__menu_item' showIcon={true} onTabChange={this.onClickTab} selected={0}
                      tabs={catStickers.map(item => { return { onlyIcon: item.onlyIcon, icon: item.icon, displayName: item.name, key: item.id }; })} />
                {/* catStickers.map(item=>{
                 return(
                 <div className="sticker_block__menu_item">
                 <div className="sticker_block__menu_name">
                 {item.name}
                 </div>
                 </div>
                 )
                 })*/}

                <div className='sticker_block__menu_add sticker_block__menu_item'>
                  <div />
                </div>

              </div>
              <div className='sticker_block__stickers'>
                {stickers.map(item => {
                  return (
                    <div onClick={(it) => this.onClickSticker(item)} key={item.id} className='sticker_block__sticker'>
                      <img className='sticker_block__sticker_img' src={item.path} />
                      <div onClick={(e, it) => this.onClickStickerInfo(e, item)} className="sticker_block__informer" >?</div>
                    </div>
                  );
                })}
              </div>
            </div>
            : null}

        </div>

      </div>
    );
  }
}