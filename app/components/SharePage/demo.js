export const stickersG = [{
  id: 1,
  uuid: null,
  path: '/assets/stickers/580b57fbd9996e24bc43be2a.png',
  cat: 1,
  popularity: 100,
},
  {
    id: 2,
    uuid: null,
    path: '/assets/stickers/588a3cc6d06f6719692a2d0a.png',
    cat: 1,
    popularity: 100,
  },
  {
    id: 3,
    uuid: null,
    path: '/assets/stickers/l3q2AuZRzjmzRie1G.gif',
    cat: 1,
    popularity: 101,
    width: 150,
    height: 150,
  },
  {
    id: 4,
    uuid: null,
    path: '/assets/stickers/ToMjGpr2JbrKkbDotSE.gif',
    cat: 1,
    popularity: 101,
  },
  {
    id: 5,
    uuid: null,
    path: '/assets/stickers/l0IyoV0i0YvzBdg64.gif',
    cat: 1,
    popularity: 101,
    width: 150,
    height: 400,
  }
];

export const catStickers =  [{
  id: 24,
  uuid: null,
  popularity: 100,
  name: 'Politics',
  slides: stickersG
},
  {
    id: 25,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 26,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 27,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 28,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 29,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 30,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 31,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 32,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 33,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 34,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
  {
    id: 35,
    uuid: null,
    popularity: 200,
    name: 'Memes',
    slides: stickersG
  },
];

export const layoutsG = [{
  id: 11,
  uuid: null,
  path: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be2a.png',
  cat: 1,
  popularity: 100,
  name: 'Poster',
  className: 'l_poster'
  },
 /* {
    id: 12,
    uuid: null,
    path: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be2a.png',
    cat: 1,
    popularity: 101,
    name: 'Trash',
    className: 'l_trash'
  },*/
  {
    id: 14,
    uuid: null,
    path: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be2a.png',
    cat: 1,
    popularity: 101,
    name: 'Right',
    className: 'l_right'
  },
  {
    id: 15,
    uuid: null,
    path: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be2a.png',
    cat: 1,
    popularity: 101,
    name: 'Bottom',
    className: 'l_bottom'
  },
  {
    id: 16,
    uuid: null,
    path: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be2a.png',
    cat: 1,
    popularity: 101,
    name: 'Read more',
    className: 'l_read_more'
  },
  {
    id: 17,
    uuid: null,
    path: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be2a.png',
    cat: 1,
    popularity: 101,
    name: 'Animated',
    className: 'l_anim'
  }
];