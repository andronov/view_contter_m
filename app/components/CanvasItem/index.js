import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import  Dropdown from '../Dropdown';
import AnimationText from "../AnimationText/index";
import {Motion, spring} from 'react-motion';

let raf = function(){
  return (
    window.requestAnimationFrame       ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function(/* function */ callback){
      window.setTimeout(callback, 1000 / 60);
    }
  );
}();

let requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame
  || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

let animations = {
  lineRightToLeft: {

  }
};

@inject("appState")
@observer
export default class CanvasItem extends React.Component {

  state = {
    canvas: null,
    ctx: null,
    drawing: false,
    anim: true,
    mouseActive: false,
    //animation: 'lineRightToLeft',
    animation: null,
    type: 'gradient',

    mouseX: 0,
    mouseY: 0,
    hover: false,

    openSettings: false,
    rectElement: {
      top: 240,
      left: 30,
      width: 430,
      height: 100
    },
    setEdit: false,

    canvasText: {
      text: 'Game of Thrones season 7 Bingo cards ',
      font: '32px Calibri',
      fontFamily: 'Calibri',
      fontSize: 32,
      fillStyle: '#ffffff'
    },

    canvasTextDesc: {
      text: 'Sunday\'s episode of "Game of Thrones" ended with an unexpected turn. The Lannister army, led by Jaime, abandoned its home of Casterly Rock and took Highgarden, while the Unsullied fought a small contingent of Lannisters.',
      font: "14px 'Titillium Web'",
      fontFamily: "'Titillium Web'",
      fontSize: 14,
      fillStyle: '#ffffff'
    },

    canvasTexts: [
      {
        text: 'Game of Thrones season 7 Bingo cards ',
        font: '24px Calibri',
        fontFamily: 'Calibri',
        fontSize: 24,
        fillStyle: '#ffffff',
        type: 'title'
      },
      {
        text: 'Sunday\'s episode of "Game of Thrones" ended with an unexpected turn. The Lannister army, led by Jaime, abandoned its home of Casterly Rock and took Highgarden, while the Unsullied fought a small contingent of Lannisters.',
        font: '16px Roboto',
        fontFamily: 'Roboto',
        fontSize: 16,
        fillStyle: '#ffffff',
        type: 'desc'
      },
    ],

    initial_list: [],
    undo_list: [],
  };

  undo = (canvas, ctx) => {
    this.restoreState(canvas, ctx, this.state.undo_list, this.state.redo_list);
  };
  redo = (canvas, ctx) => {
    this.restoreState(canvas, ctx, this.state.redo_list, this.state.undo_list);
  };

  saveState = (canvas, list, keep_redo) => {
    keep_redo = keep_redo || false;
    if(!keep_redo) {
      this.redo_list = [];
    }

    (list || this.undo_list).push(canvas.toDataURL());
  };

  restoreState = () => {
    let { initial_list: pop, canvas, ctx } = this.state;
    if(pop.length) {
      let restore_state = pop[0];
      let img = new Element('img', {'src':restore_state});
      img.onload = function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
      }
    }
  };

  componentDidUpdate(prevProps) {
    if(prevProps.animCounter !== this.props.animCounter) {
      //console.log('componentDidUpdate', prevProps, this.props.animCounter, prevProps.animCounter);
      this.setState({anim: true});
      setTimeout(()=>{
        this.setAnimation()
      }, 10);
    }
  };

  componentDidMount(){
    let { name } = this.props;
    let that = this;
    let canvas = document.querySelector(`#${name}`);
  //  let canvas_st = document.querySelector(`#${name}_st`);
    this.setState({
      canvas: canvas, ctx: canvas.getContext('2d')
    });
    setTimeout(()=>{
      /*(function animloop(){
        requestAnimFrame(animloop);
        that.setAnimationLoop();
      })();*/
      this.setAnimation()
    }, 10);
  }


  setAnimationNew = () => {
    this.animate()
  };

  animate = (inverse, duration) => {
    let { canvas, ctx } = this.state;
    duration = duration || 800;

    //настройки отображения
    let settings = {
      topText:{color:'#ffffff', backgroundColor:'transparent', fontFamily:'Verdana', text:'These LED eyelashes are future fashion'}
      ,bottomText:{color:'#ffffff', backgroundColor:'transparent', fontFamily:'Verdana', text:'Sometimes you see a product', scale: 0.6}
      ,lines:{color:'#ffffff', glareColor:'#e22626'}
    };

    //задаю рамки в которых должна проиграть анимация
    //по дефолту это размер canvas
    let textEffectBoundingBox = {x: 100, y: 100, width: canvas.width, height: 100};

    //описание анимаций
    //fromTo - это какой параметр твинится и относительно какого значения textEffectBoundingBox (т.е fromTo:{x:{from:0, to:.5, by:'width'}} означает что траектория движения от 0 до середины bonndingbox)
    //easing - это математика движения для элемента. т.е анимация появления описано в поле easeInCirc (допустимые значения: linear,easeInSine,easeOutSine,easeInOutSine,easeInCirc,easeOutCirc,easeOutBack,easeOutBack)
    //element.draw - функция отрисовки
    let animations = [
      {fromTo:{x:{from:0, to:.5, by:'width'}}, easing:{normal:'easeOutBack', inverse:'easeInCirc'}, element:{
        draw:function () {
          let s = settings.topText;

          let fs = fontSize * (s.scale || 1);
          if(s.backgroundColor){
            ctx.fillStyle = s.backgroundColor;
            ctx.fillRect(this.x - maxTextWidth * .5, textEffectBoundingBox.height * .5 - fs - fs * .2, maxTextWidth, fs + fs * .2);
          }
          ctx.save();
          //ctx.shadowOffsetX = 0; ctx.shadowOffsetY = 0; ctx.shadowColor = "rgba(0,0,0,0.6)"; ctx.shadowBlur = 2;
          ctx.fillStyle = s.color;
          ctx.textBaseline = "bottom"; ctx.textAlign = "center";
          ctx.font = fs + 'px ' + s.fontFamily;
          ctx.fillText(s.text, this.x, textEffectBoundingBox.height * .5);
          ctx.restore();
        }}
      }
      ,{fromTo:{x:{from:0, to:.5, by:'width'}}, easing:{normal:'easeOutBack', inverse:'easeInCirc'}, element:{
        draw:function () {
          let s = settings.bottomText;

          let fs = fontSize * (s.scale || 1);
          if(s.backgroundColor){
            ctx.fillStyle = s.backgroundColor;
            ctx.fillRect(this.x - maxTextWidth * .5, textEffectBoundingBox.height * .5 , maxTextWidth, fs + fs * .2);
          }
          ctx.save();
          //ctx.shadowOffsetX = 0; ctx.shadowOffsetY = 0; ctx.shadowColor = "rgba(0,0,0,0.6)"; ctx.shadowBlur = 2;
          ctx.fillStyle = s.color;
          ctx.textBaseline = "top"; ctx.textAlign = "center";
          ctx.font = fs + 'px ' + s.fontFamily;
          ctx.fillText(s.text, this.x, textEffectBoundingBox.height * .5);
          ctx.restore();
        }}
      }
      ,{fromTo:{x:{from:1, to:0, by:'width'}, width:{from:.2, to:.05, by:'width'}, glareWidth:{from:1.5, to:0, by:'width'} }, easing:{normal:'easeOutCirc', inverse:'easeInCirc'}, element:{
        draw:function () {
          let s = settings.lines;

          ctx.save();
          ctx.clearRect(0, 0, this.x, textEffectBoundingBox.height);
          ctx.fillStyle = s.color;
          ctx.fillRect(this.x, textEffectBoundingBox.height * 0.1, this.width, textEffectBoundingBox.height * 0.8);
          ctx.fillStyle = s.glareColor;
          let glareVertStroke = Math.min(this.glareWidth, this.width * .4);
          let glareHorizStroke = Math.min(this.glareWidth, this.width * .1);
          let glareX = this.x + this.width - glareVertStroke;
          ctx.fillRect(glareX, textEffectBoundingBox.height * 0.1, glareVertStroke, textEffectBoundingBox.height * 0.8);

          let x, y;
          ctx.beginPath();
          x = glareX;
          y = textEffectBoundingBox.height * 0.1;
          ctx.moveTo(x, y);
          ctx.lineTo(x + this.glareWidth, y);
          ctx.lineTo(x, y + glareHorizStroke);
          ctx.closePath();
          ctx.fill();
          y = textEffectBoundingBox.height * 0.9;
          ctx.moveTo(x, y);
          ctx.lineTo(x + this.glareWidth, y);
          ctx.lineTo(x, y - glareHorizStroke);
          ctx.closePath();
          ctx.fill();

          ctx.restore();
        }}
      }

    ];

    let easing = {
      linear: function (t, b, c, d) {return c * t / d + b;}
      ,easeInSine: function (t, b, c, d) {return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;}
      ,easeOutSine: function (t, b, c, d) {return c * Math.sin(t / d * (Math.PI / 2)) + b;}
      ,easeInOutSine: function (t, b, c, d) {return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;}
      ,easeInCirc: function (t, b, c, d) {return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;}
      ,easeOutCirc: function (t, b, c, d) {return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;}
      ,easeInBack: function (t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
      }
      ,easeOutBack: function (t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
      }
    };

    //вычисляю максимальный размер шрифта
    let fontSize = textEffectBoundingBox.height * .4;
    let maxTextWidth = textEffectBoundingBox.width * .8;
    let topTextWidth = 0;
    let bottomTextWidth = 0;
    do{
      fontSize--;
      let s = settings.topText;
      ctx.font =  fontSize * (s.scale || 1) + 'px ' + s.fontFamily;
      let topTextMetrics = ctx.measureText(s.text);
      topTextWidth = topTextMetrics.width;

      s = settings.bottomText;
      ctx.font =  fontSize * (s.scale || 1) + 'px ' + s.fontFamily;
      let bottomTextMetrics = ctx.measureText(s.text);
      bottomTextWidth = bottomTextMetrics.width;
    }while (topTextWidth > maxTextWidth || bottomTextWidth > maxTextWidth);



    let startTime;
    function loop(ts) {
      let runtime = ts - startTime;

      if(runtime > duration){
        runtime = duration;
      }

      ctx.clearRect(0, 0, canvas.width, canvas.height);

      animations.forEach(function (anim) {
        let element = anim.element;
        Object.keys(anim.fromTo).forEach(function (key) {
          let prop = anim.fromTo[key];
          let by = textEffectBoundingBox[prop.by] || 1;
          let from = (inverse ? prop.to : prop.from) * by;
          let to = (inverse ? prop.from : prop.to) * by;

          element[key] = easing[anim.easing[(inverse ? 'inverse': 'normal')]](runtime, from, to-from, duration);
        });
        element.draw();
      });

      if (runtime < duration) {
        requestAnimationFrame(loop);
      }
    }

    requestAnimationFrame(function(ts){
      startTime = ts;
      loop(ts);
    });

  };

  setAnimation = () => {
    let { canvas, ctx, initial_list, canvasText,  anim} = this.state;
    let { item, itemLinkColors, appState, type } = this.props;
    if(!canvas)return;
    console.log('setAnimtain');

    let windowW = canvas.width;
    let windowH = canvas.height;
    ctx.clearRect(0, 0, windowW, windowH);

    return this.setAnimationNew()

    //bg element
    if(type.startsWith('gradient')){
      this.setGradient();
      return;
    }
    if(type.startsWith('text')){
      this.setText();
      return;
    }

    if(type.startsWith('animation')){
      this.setAnimationMouse()
      return;
    }
    //ctx.fillStyle = 'blue';

    let w = 2;
    let h = 50;
    let xPos = windowW;
    let that = this;
    let a = anim;

    let i = 0;
    let texts = canvasText.text;


    let fontSize = 24,
      width = windowW-60,
      lines = [],
      line = '',
      lineTest = '',
      currentY = 0;

    ctx.font = fontSize + 'px '+ canvasText.fontFamily;

    texts.split('').forEach(word=>{
      lineTest = line + word;

      // Check total width of line or last word
      if (ctx.measureText(lineTest).width > width) {
        // Calculate the new height
        currentY = lines.length * fontSize + fontSize;

        // Record and reset the current line
        lines.push({ text: line, height: currentY });
        line = word;
      } else {
        line = lineTest;
      }
    });

    if (line.length > 0) {
      currentY = lines.length * fontSize + fontSize;
      lines.push({ text: line.trim(), height: currentY });
    }

    let draw = function () {
      if(!a){
        return;
      }
      ctx.clearRect(0, 0, windowW, windowH);
      //ctx.translate(incr, 0);
      //ctx.fillStyle = `#${itemLinkColor}`;
      ctx.fillStyle = itemLinkColors.length ? '#'+itemLinkColors[0] : "rgba(45, 47, 58, 0.7)";
      ctx.fillRect(xPos-5, (windowH-(lines.length*fontSize)+5)-25, 600, h);
      // ctx.fill();

      ctx.font = `${canvasText.fontSize} ${canvasText.fontFamily}`;
      ctx.fillStyle = canvasText.fillStyle;
      //ctx.fillText("Wimbledon: how does the weather", 10, h-70);
      //ctx.fillText("affect things?", 10, h-40);

      //let t = 'All the world \'s a stage, and all the men and women merely players. They have their exits and their entrances; And one man in his time plays many parts.';
      //that.wrapText(ctx, t, xPos+30, windowH-(h/2)-18, windowW, 24);
      //ctx.fillText(texts.slice(0, i), xPos+30, windowH-(h/2)-18);
      lines.forEach(line=>{
        ctx.fillText(line.text, xPos, windowH-line.height);
      });

      xPos -= 10;
      i += 2;
      //console.log('xPos',  that);
      if(xPos < 30){
        a = false;
        that.setState({anim: false, initial_list: [canvas.toDataURL()]})
      }
      requestAnimFrame(draw)
    };

    requestAnimFrame(draw);
  };

  onMouseMoveCanvas = (e) => {
    let rect = e.target.getBoundingClientRect();
    let x = e.clientX - rect.left;
    let y = e.clientY - rect.top;
    let { rectElement, setEdit } = this.state;
    let { top, left, width, height } = rectElement;

    this.setState({mouseX: x, mouseY: y});

    if(left < x && top < y  && (left+width) > x && (top+height) > y){
      if(!setEdit){
        this.setState({setEdit: true});
       // ctx_st.strokeStyle = "rgba(255, 255, 255, 0.5)";
       // ctx_st.lineWidth = 2;
       // ctx_st.strokeRect(left, top, width, height);
      }
     // console.log('yesss')
    }
    else{
      this.setState({setEdit: false});
      //ctx_st.clearRect(0,0, canvas_st.width, canvas_st.height)
    }
    //console.log('onMouseMoveCanvas', x, y)
  };

  setItemLayout = (e) => {
    if(!e)return;
    e.style.zIndex = this.props.appState.setItemLayout(e);
  };

  onMouseDownCanvas = (e) => {
    let { canvas, ctx, initial_list, canvasText,  anim} = this.state;
    let windowW = canvas.width;
    let windowH = canvas.height;
    this.setState({mouseActive: true})
  };

  onMouseLeaveCanvas = (e) => {
    this.setState({hover: false})
  };

  onMouseEnterCanvas = (e) => {
    this.setState({hover: true})
  };

  render() {
    let { name, appState } = this.props;
    let { openSettings, setEdit, canvasText,  rectElement } = this.state;
    let { top, left, width, height } = rectElement;

    return (
      <div style={{zIndex: 35}}
           className="object__anim" ref={this.setItemLayout}>

        <canvas id={name}
                onMouseDown={this.onMouseDownCanvas}
                onMouseMove={this.onMouseMoveCanvas}
                onMouseLeave={this.onMouseLeaveCanvas}
                onMouseEnter={this.onMouseEnterCanvas}
                className={name} width={appState.size.width}/>
      </div>
    );
  }
}
