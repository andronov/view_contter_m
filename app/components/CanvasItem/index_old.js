import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import  Dropdown from '../Dropdown';
import AnimationText from "../AnimationText/index";
import {Motion, spring} from 'react-motion';

let raf = function(){
  return (
    window.requestAnimationFrame       ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function(/* function */ callback){
      window.setTimeout(callback, 1000 / 60);
    }
  );
}();

let animations = {
  lineRightToLeft: {

  }
};

@inject("appState")
@observer
export default class CanvasItem extends React.Component {

  state = {
    canvas: null,
    ctx: null,
    drawing: false,
    anim: true,
    mouseActive: false,
    //animation: 'lineRightToLeft',
    animation: null,
    type: 'gradient',

    mouseX: 0,
    mouseY: 0,
    hover: false,

    openSettings: false,
    rectElement: {
      top: 240,
      left: 30,
      width: 430,
      height: 100
    },
    setEdit: false,

    canvasText: {
      text: 'Game of Thrones season 7 Bingo cards ',
      font: '32px Calibri',
      fontFamily: 'Calibri',
      fontSize: 32,
      fillStyle: '#ffffff'
    },

    canvasTextDesc: {
      text: 'Sunday\'s episode of "Game of Thrones" ended with an unexpected turn. The Lannister army, led by Jaime, abandoned its home of Casterly Rock and took Highgarden, while the Unsullied fought a small contingent of Lannisters.',
      font: "14px 'Titillium Web'",
      fontFamily: "'Titillium Web'",
      fontSize: 14,
      fillStyle: '#ffffff'
    },

    canvasTexts: [
      {
        text: 'Game of Thrones season 7 Bingo cards ',
        font: '24px Calibri',
        fontFamily: 'Calibri',
        fontSize: 24,
        fillStyle: '#ffffff',
        type: 'title'
      },
      {
        text: 'Sunday\'s episode of "Game of Thrones" ended with an unexpected turn. The Lannister army, led by Jaime, abandoned its home of Casterly Rock and took Highgarden, while the Unsullied fought a small contingent of Lannisters.',
        font: '16px Roboto',
        fontFamily: 'Roboto',
        fontSize: 16,
        fillStyle: '#ffffff',
        type: 'desc'
      },
    ],

    initial_list: [],
    undo_list: [],
  };

  undo = (canvas, ctx) => {
    this.restoreState(canvas, ctx, this.state.undo_list, this.state.redo_list);
  };
  redo = (canvas, ctx) => {
    this.restoreState(canvas, ctx, this.state.redo_list, this.state.undo_list);
  };

  saveState = (canvas, list, keep_redo) => {
    keep_redo = keep_redo || false;
    if(!keep_redo) {
      this.redo_list = [];
    }

    (list || this.undo_list).push(canvas.toDataURL());
  };

  restoreState = () => {
    let { initial_list: pop, canvas, ctx } = this.state;
    if(pop.length) {
      let restore_state = pop[0];
      let img = new Element('img', {'src':restore_state});
      img.onload = function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
      }
    }
  };

  componentDidUpdate(prevProps) {
    if(prevProps.animCounter !== this.props.animCounter) {
      //console.log('componentDidUpdate', prevProps, this.props.animCounter, prevProps.animCounter);
      this.setState({anim: true});
      setTimeout(()=>{
        this.setAnimation()
      }, 10);
    }
  };

  componentDidMount(){
    let { name } = this.props;
    let that = this;
    let canvas = document.querySelector(`#${name}`);
    //  let canvas_st = document.querySelector(`#${name}_st`);
    this.setState({
      canvas: canvas, ctx: canvas.getContext('2d')
    });
    setTimeout(()=>{
      /*(function animloop(){
       requestAnimFrame(animloop);
       that.setAnimationLoop();
       })();*/
      this.setAnimation()
    }, 10);
  }

  setGradient = () => {
    let { canvas, ctx} = this.state;
    let { itemLinkColors, appState } = this.props;

    let windowW = canvas.width;
    let windowH = canvas.height;

    ctx.clearRect(0, 0, windowW, windowH);
    let gradient = ctx.createLinearGradient(0, windowH/2, 0, windowH-100);
    gradient.addColorStop(0, "transparent");
    //gradient.addColorStop(0.3, 'rgba(51, 109, 255, 0.2)');
    gradient.addColorStop(0.1, appState.to_rgba(itemLinkColors.length ? '#'+itemLinkColors[0] :'#cc322e', '0.1'));
    gradient.addColorStop(1, appState.to_rgba(itemLinkColors.length ? '#'+itemLinkColors[0] :'#cc322e'));
    ctx.fillStyle = gradient;
    ctx.fillRect(0, 0, windowW, windowH);
  };

  getLineFromText = (ctx, width, fontSize, texts) => {
    let line = '',
      lines = [],
      lineTest = '',
      heightEl = 0,
      currentY = 0;

    ctx.font = fontSize + 'px Arial';
    texts.split(' ').forEach(word => {
      if (line == '') {
        lineTest = line + word;
      } else {
        lineTest = line + ' ' + word;
      }

      // Check total width of line or last word
      if (ctx.measureText(lineTest).width > width) {
        // Calculate the new height
        currentY = lines.length * fontSize + fontSize;

        // Record and reset the current line
        heightEl = heightEl ? heightEl : currentY;
        lines.push({text: line, height: currentY, heightEl, widthLine: ctx.measureText(line).width});
        line = word;
      } else {
        line = lineTest;
      }
    });


    if (line.length > 0) {
      currentY = lines.length * fontSize + fontSize;
      heightEl = heightEl ? heightEl : currentY;
      lines.push({text: line.trim(), height: currentY, heightEl, widthLine: ctx.measureText(line.trim()).width});
    }

    return lines;
  };

  setText = () => {
    let { canvas, ctx} = this.state;
    let { canvasText, canvasTextDesc } = this.props.store;
    let { itemLinkColors, appState, type, with_type } = this.props;

    let windowW = canvas.width;
    let windowH = canvas.height;
    let lines = [],
      lastY = windowH-20,
      offset = 20,
      fontSize = 16;
    if(with_type === 'gradient_horizontal'){
      lastY = 10;
      offset = 10;
      if(canvasText) {
        fontSize = canvasText.fontSize;
        ctx.font = fontSize + 'px ' + canvasText.fontFamily;
        ctx.fillStyle = canvasText.fillStyle;
        lines = this.getLineFromText(ctx, (windowW/2)-50, fontSize, canvasText.text);
        lines.forEach(line => {
          ctx.fillText(line.text, offset, lastY+line.heightEl);
          lastY += line.heightEl;
        });
      }
      if(canvasTextDesc){
        lastY += 50;
        fontSize = canvasTextDesc.fontSize;
        ctx.font = fontSize + 'px '+ canvasTextDesc.fontFamily;
        ctx.fillStyle = canvasTextDesc.fillStyle;
        lines = this.getLineFromText(ctx, (windowW/2)-50, fontSize, canvasTextDesc.text);
        lines.forEach(line=>{
          ctx.fillText(line.text, offset, lastY);
          lastY += line.heightEl+5;
        });
      }
      return
    }

    if(canvasTextDesc){
      fontSize = canvasTextDesc.fontSize;
      ctx.font = fontSize + 'px '+ canvasTextDesc.fontFamily;
      ctx.fillStyle = canvasTextDesc.fillStyle;
      lines = this.getLineFromText(ctx, windowW-60, fontSize, canvasTextDesc.text);
      lines.reverse().forEach(line=>{
        ctx.fillText(line.text, offset, lastY);
        lastY -= line.heightEl+5;
      });
    }
    if(canvasText) {
      lastY -= 10;
      fontSize = canvasText.fontSize;
      ctx.font = fontSize + 'px ' + canvasText.fontFamily;
      ctx.fillStyle = canvasText.fillStyle;
      lines = this.getLineFromText(ctx, windowW - 60, fontSize, canvasText.text);
      lines.reverse().forEach(line => {
        ctx.fillText(line.text, offset, lastY);
        lastY -= line.heightEl;
      });
    }
  };

  setAnimationMouse = () => {
    let { canvas, ctx, mouseX: x, mouseY: y, hover} = this.state;
    let { item, itemLinkColors, appState, type, store } = this.props;
    let { canvasText, canvasTextDesc } = store;

    let windowW = canvas.width;
    let windowH = canvas.height;
    ctx.clearRect(0, 0, windowW, windowH);

    ctx.font = canvasText.fontSize + 'px '+ canvasText.fontFamily;
    ctx.fillStyle = canvasText.fillStyle;


    this.o = this.o ? this.o : 50;
    if(hover && this.o < 100){
      this.o += 2;
    }
    if(!hover && this.o > 50){
      this.o -= 2;
    }

    console.log('hover', hover, this.o);

    let lines = [],
      lastY = (windowH+80)-this.o,
      offset = 20,
      fontSize = 16;

    if(canvasTextDesc){
      fontSize = canvasTextDesc.fontSize;
      ctx.font = fontSize + 'px '+ canvasTextDesc.fontFamily;
      ctx.fillStyle = canvasTextDesc.fillStyle;
      lines = this.getLineFromText(ctx, windowW-60, fontSize, canvasTextDesc.text);
      lines.reverse().forEach(line=>{
        ctx.fillText(line.text, offset, lastY);
        lastY -= line.heightEl+5;
      });
    }

    if(canvasText) {
      lastY -= 20;
      fontSize = canvasText.fontSize;
      ctx.font = fontSize + 'px ' + canvasText.fontFamily;
      ctx.fillStyle = canvasText.fillStyle;
      lines = this.getLineFromText(ctx, windowW - 60, fontSize, canvasText.text);
      lines.reverse().forEach(line => {
        ctx.fillText(line.text, offset, lastY);
        lastY -= line.heightEl;
      });
    }



    //ctx.fillText(canvasText.text, 50, windowH-this.o);
  };

  setAnimationLoop = () => {
    //console.log('mouseActive', this.state.mouseActive);
    this.setAnimationMouse();
  };

  setAnimationNew = () => {
    let { canvas, ctx: context } = this.state;

    let paddingCanvas = 50,
      lineWhiteWidth = 14,
      lineWhiteX = paddingCanvas,
      lineWhiteMargin = 5,
      marginLeftText = paddingCanvas + lineWhiteWidth + lineWhiteMargin,
      maxWidthBlueText = canvas.width - paddingCanvas * 2 - lineWhiteWidth - lineWhiteMargin,
      blueBoxPadding = 10,
      fontSizeBlueText = 16,
      blueText = 'These LED eyelashes are future fashion',
      // blueText = 'Lorem ipsum dolor',
      heightBlueBox = this.getLineFromText(context, canvas.width - marginLeftText - blueBoxPadding * 2, fontSizeBlueText, blueText),
      maxWidthLineBlueBox = Math.max.apply(Math, heightBlueBox.map(w => w.widthLine)),
      heightBlueBoxSum = heightBlueBox.length * fontSizeBlueText * 1.2 + blueBoxPadding * 2,
      greenText = 'Then you remember you’re living in 2017',
      greenBoxX = -(marginLeftText),
      greenBoxPadding = 10,
      fontSizeGreenText = 12,
      heightGreenBox = this.getLineFromText(context, canvas.width - marginLeftText - greenBoxPadding * 2, fontSizeGreenText, greenText),
      greenBoxWidth = Math.max.apply(Math, heightGreenBox.map(w => w.widthLine)),
      heightGreenBoxSum = heightGreenBox.length * fontSizeGreenText * 1.2 + greenBoxPadding * 2,
      lineWhiteEndHeight = heightBlueBoxSum + lineWhiteMargin + heightGreenBoxSum,
      lineWhiteStartHeight = lineWhiteEndHeight * 2 / 3,
      lineWhiteY = (lineWhiteEndHeight - lineWhiteStartHeight) / 2 + paddingCanvas,
      blueBoxX = -(maxWidthLineBlueBox / 2);


    let runDrawLine = true,
      runDrawBLueBox = false,
      runDrawGreenBox = false;

    console.log(marginLeftText);
    console.log(blueBoxX);
// console.log(heightBlueBoxSum);
// console.log(heightGreenBoxSum);
    window.frames = [];

    function drawLine() {
      if (runDrawLine) {
        context.clearRect(0, 0, canvas.width, canvas.height);

        context.fillStyle = '#ffffff';
        context.fillRect(lineWhiteX, lineWhiteY, lineWhiteWidth, lineWhiteStartHeight);
        lineWhiteStartHeight = lineWhiteStartHeight + 2;
        lineWhiteY = lineWhiteY - 1;

        if (lineWhiteStartHeight >= lineWhiteEndHeight - 2) {
          runDrawLine = false;
          runDrawBLueBox = true;
          requestAnimationFrame(drawBLueBox);
        } else {
          requestAnimationFrame(drawLine);
        }
        window.frames.push(canvas.toDataURL('image/png'));
      }
    }

    function drawStep() {
      context.clearRect(0, 0, canvas.width, canvas.height);

      context.fillStyle = '#c92032';
      if (heightBlueBox.length < 2) {
        // context.font = fontSizeBlueText + 'px Arial';
        // console.log(context.measureText(blueText).width);
        context.fillRect(blueBoxX, paddingCanvas, maxWidthLineBlueBox + blueBoxPadding * 2, heightBlueBox.length * fontSizeBlueText * 1.2 + blueBoxPadding * 2);
      } else {
        context.fillRect(blueBoxX, paddingCanvas, maxWidthLineBlueBox + blueBoxPadding * 2, heightBlueBox.length * fontSizeBlueText * 1.2 + blueBoxPadding * 2);
      }
      context.fillStyle = '#fff';
      // context.textAlign = 'center';
      context.textBaseline = 'top';
      context.font = fontSizeBlueText + 'px Arial';

      if (heightBlueBox.length < 2) {
        context.fillText(blueText, blueBoxX + blueBoxPadding, paddingCanvas + blueBoxPadding);
      } else {
        context.fillText(heightBlueBox[0].text, blueBoxX + blueBoxPadding, paddingCanvas + blueBoxPadding);
        for (var i = 1, len = heightBlueBox.length; i < len; i++) {
          context.fillText(heightBlueBox[i].text, blueBoxX + blueBoxPadding, heightBlueBox[i - 1].height * 1.2 + paddingCanvas + blueBoxPadding);
        }
      }

      if (runDrawGreenBox) {
        context.fillStyle = '#2ecc70';
        context.fillRect(greenBoxX, paddingCanvas + lineWhiteMargin + heightBlueBoxSum, greenBoxWidth + greenBoxPadding * 2, heightGreenBoxSum);
        context.fillStyle = '#fff';
        context.font = fontSizeGreenText + 'px Arial';
        context.fillText(greenText, greenBoxX + greenBoxPadding, paddingCanvas + lineWhiteMargin + heightBlueBoxSum + greenBoxPadding);

      }

      context.clearRect(0, 0, marginLeftText, canvas.height);

      context.fillStyle = '#ffffff';
      context.fillRect(lineWhiteX, paddingCanvas, lineWhiteWidth, lineWhiteEndHeight);
    }

    function drawBLueBox() {
      if (runDrawBLueBox) {
        drawStep();

        runDrawGreenBox = true;
        if (blueBoxX >= (marginLeftText - 9)) {
          blueBoxX = marginLeftText;
        } else {
          blueBoxX = blueBoxX + 10;
        }

        if (greenBoxX >= (marginLeftText - 4)) {
          greenBoxX = marginLeftText;
          // runDrawGreenBox = false;
        } else {
          runDrawGreenBox = true;
          greenBoxX = greenBoxX + 5;
        }

        if (blueBoxX >= marginLeftText && greenBoxX >= marginLeftText) {
          drawStep();
          runDrawBLueBox = false;
        }

        requestAnimationFrame(drawBLueBox);
      }
    }

    drawLine();
    //canvas.toDataURL('image/png');

  };

  setAnimation = () => {
    let { canvas, ctx, initial_list, canvasText,  anim} = this.state;
    let { item, itemLinkColors, appState, type } = this.props;
    if(!canvas)return;
    console.log('setAnimtain');

    let windowW = canvas.width;
    let windowH = canvas.height;
    ctx.clearRect(0, 0, windowW, windowH);

    return this.setAnimationNew()

    //bg element
    if(type.startsWith('gradient')){
      this.setGradient();
      return;
    }
    if(type.startsWith('text')){
      this.setText();
      return;
    }

    if(type.startsWith('animation')){
      this.setAnimationMouse()
      return;
    }
    //ctx.fillStyle = 'blue';

    let w = 2;
    let h = 50;
    let xPos = windowW;
    let that = this;
    let a = anim;

    let i = 0;
    let texts = canvasText.text;


    let fontSize = 24,
      width = windowW-60,
      lines = [],
      line = '',
      lineTest = '',
      currentY = 0;

    ctx.font = fontSize + 'px '+ canvasText.fontFamily;

    texts.split('').forEach(word=>{
      lineTest = line + word;

      // Check total width of line or last word
      if (ctx.measureText(lineTest).width > width) {
        // Calculate the new height
        currentY = lines.length * fontSize + fontSize;

        // Record and reset the current line
        lines.push({ text: line, height: currentY });
        line = word;
      } else {
        line = lineTest;
      }
    });

    if (line.length > 0) {
      currentY = lines.length * fontSize + fontSize;
      lines.push({ text: line.trim(), height: currentY });
    }

    let draw = function () {
      if(!a){
        return;
      }
      ctx.clearRect(0, 0, windowW, windowH);
      //ctx.translate(incr, 0);
      //ctx.fillStyle = `#${itemLinkColor}`;
      ctx.fillStyle = itemLinkColors.length ? '#'+itemLinkColors[0] : "rgba(45, 47, 58, 0.7)";
      ctx.fillRect(xPos-5, (windowH-(lines.length*fontSize)+5)-25, 600, h);
      // ctx.fill();

      ctx.font = `${canvasText.fontSize} ${canvasText.fontFamily}`;
      ctx.fillStyle = canvasText.fillStyle;
      //ctx.fillText("Wimbledon: how does the weather", 10, h-70);
      //ctx.fillText("affect things?", 10, h-40);

      //let t = 'All the world \'s a stage, and all the men and women merely players. They have their exits and their entrances; And one man in his time plays many parts.';
      //that.wrapText(ctx, t, xPos+30, windowH-(h/2)-18, windowW, 24);
      //ctx.fillText(texts.slice(0, i), xPos+30, windowH-(h/2)-18);
      lines.forEach(line=>{
        ctx.fillText(line.text, xPos, windowH-line.height);
      });

      xPos -= 10;
      i += 2;
      //console.log('xPos',  that);
      if(xPos < 30){
        a = false;
        that.setState({anim: false, initial_list: [canvas.toDataURL()]})
      }
      requestAnimFrame(draw)
    };

    requestAnimFrame(draw);
  };

  onMouseMoveCanvas = (e) => {
    let rect = e.target.getBoundingClientRect();
    let x = e.clientX - rect.left;
    let y = e.clientY - rect.top;
    let { rectElement, setEdit } = this.state;
    let { top, left, width, height } = rectElement;

    this.setState({mouseX: x, mouseY: y});

    if(left < x && top < y  && (left+width) > x && (top+height) > y){
      if(!setEdit){
        this.setState({setEdit: true});
        // ctx_st.strokeStyle = "rgba(255, 255, 255, 0.5)";
        // ctx_st.lineWidth = 2;
        // ctx_st.strokeRect(left, top, width, height);
      }
      // console.log('yesss')
    }
    else{
      this.setState({setEdit: false});
      //ctx_st.clearRect(0,0, canvas_st.width, canvas_st.height)
    }
    //console.log('onMouseMoveCanvas', x, y)
  };

  setItemLayout = (e) => {
    if(!e)return;
    e.style.zIndex = this.props.appState.setItemLayout(e);
  };

  onChangeAnimText = (e) => {
    //console.log('onChangeAnimText', e.target.value);
    let { canvasText } = this.state;
    canvasText['text'] = e.target.value;
    this.setState({canvasText: canvasText})
  };

  stClose = () => {
    this.setState({openSettings: false});
    this.setState({anim: true});
    setTimeout(()=>{
      this.setAnimation()
    }, 10);
  };

  setFontFamily = (font) => {
    let { canvasText } = this.state;
    canvasText['fontFamily'] = font.fontFamily;
    this.setState({canvasText: canvasText})
  };

  onMouseDownCanvas = (e) => {
    let { canvas, ctx, initial_list, canvasText,  anim} = this.state;
    let windowW = canvas.width;
    let windowH = canvas.height;
    this.setState({mouseActive: true})
  };

  onMouseLeaveCanvas = (e) => {
    this.setState({hover: false})
  };

  onMouseEnterCanvas = (e) => {
    this.setState({hover: true})
  };

  render() {
    let { name } = this.props;
    let { openSettings, setEdit, canvasText,  rectElement } = this.state;
    let { top, left, width, height } = rectElement;

    return (
      <div style={{zIndex: 35}}
           className="object__anim" ref={this.setItemLayout}>


        <Motion style={{x: spring(openSettings ? 0 : -200),
          y: spring(200)}}>
          {({x, y}) =>
            <div className='st' style={{
              width: `${y}px`, /* drawOpen ? '80px': '200px',*/
              WebkitTransform: `translate3d(${x}px, 0, 0)`,
              transform: `translate3d(${x}px, 0, 0)`,
            }}>
              <div className="st__close" onClick={this.stClose}/>
              <div className='st__inner'>
                text

              </div>
            </div>

          }
        </Motion>

        { openSettings && 2 === 6 &&
        <div className="st">
          <div className="st__close" onClick={this.stClose}/>

          <div className="st__inner">sd
            {/*<div className="st__txtarea">
             <textarea
             onChange={this.onChangeAnimText}
             style={{
             font: canvasText.font,
             fontFamily: canvasText.fontFamily,
             color: canvasText.fillStyle,
             width: `${width}px`
             }}
             className="st__textarea"
             value={canvasText.text}/>

             </div>

             <div className="st__font">
             <Dropdown
             setFontFamily={this.setFontFamily}
             style={{
             font: canvasText.font,
             color: canvasText.fillStyle,
             fontFamily: canvasText.fontFamily,
             width: `${width}px`
             }}/>
             </div>

             <div className="st__anim">
             <div className="st__anim_inner">
             <div
             className="st__anim_item"
             >
             <AnimationText />
             </div>

             <div
             className="st__anim_item"
             style={{
             font: canvasText.font,
             color: canvasText.fillStyle,
             fontFamily: canvasText.fontFamily,
             width: `${width}px`
             }}>
             {canvasText.text}
             </div>

             <div
             className="st__anim_item"
             style={{
             font: canvasText.font,
             color: canvasText.fillStyle,
             fontFamily: canvasText.fontFamily,
             width: `${width}px`
             }}>
             {canvasText.text}
             </div>

             <div
             className="st__anim_item"
             style={{
             font: canvasText.font,
             color: canvasText.fillStyle,
             fontFamily: canvasText.fontFamily,
             width: `${width}px`
             }}>
             {canvasText.text}
             </div>

             </div>

             </div>*/}
          </div>
        </div>
        }


        {/*<div
         style={{width: '650px', height: '366px'}}
         className="cnv_st">
         {
         setEdit ?
         <div
         style={{top: `${top}px`, left: `${left}px`, width: `${width}px`, height: `${height}px`}}
         className="cnv_st__br">

         <div className="cnv_st__move"/>
         <div className="cnv_st__st" onClick={()=>{this.setState({openSettings: true})}}/>

         </div>
         : null
         }
         </div>*/}

        <canvas id={name}
                onMouseDown={this.onMouseDownCanvas}
                onMouseMove={this.onMouseMoveCanvas}
                onMouseLeave={this.onMouseLeaveCanvas}
                onMouseEnter={this.onMouseEnterCanvas}
                className={name} height={'366px'} width={'650px'}/>
      </div>
    );
  }
}
