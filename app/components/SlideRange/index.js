import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Draggable from 'react-draggable';

@inject("appState")
@observer
export default class SlideRange extends React.Component {

  componentDidMount (){
    this.setBrushStyle();
    console.log('slide_drag', this.refs.slide_drag)
  }

  setBrushStyle(){
    let node = document.querySelector('.draggable-button');
    node.style.width = `${this.props.brushSize}px`;
    node.style.height = `${this.props.brushSize}px`;
    node.style.backgroundColor = this.props.brushColor;
  }

  componentDidUpdate(previousProps, previousState){
    if(previousProps.brushColor !== this.props.brushColor){
      this.setBrushStyle()
    }

  }

  state = {
  };
  //#ff1d0f

  handleDrag = (e, item) => {
    let { y, node } = item;
    let h = node.parentNode.childNodes[0].offsetHeight - (node.offsetHeight/2);
    let p = y;
    if(h<p){p = 100}
    if(p < 0){p = 0}

    if(p<7){p=7}
    if(p>50){p=50}

    let res = y*100/h;

    node.style.width = `${p}px`;
    node.style.height = `${p}px`;
    node.style.backgroundColor = this.props.brushColor;
    //this.props.onSetBrushSize(p);
    //console.log('handleDragItem', res/10,res, h,y, p)
  };

  render() {

    return (
      <div className="map-slider">
        <div className="buttons">

          <div className="drag-line">
            <div className="line"/>
            <Draggable
              refs="slide_drag"
              onDrag={this.handleDrag}
              bounds="parent" axis="y">
              <div className="draggable-button"/>
            </Draggable>
          </div>
        </div>
      </div>
    );
  }
}
