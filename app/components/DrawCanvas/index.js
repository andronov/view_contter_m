import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

let points = [];

@inject("appState")
@observer
export default class DrawCanvas extends React.Component {

  state = {
    canvas: null,
    ctx: null,
    drawing: false
  };

  componentWillMount() {
    window.addEventListener('mousemove', this.handleOnMouseMoveBlock);
    window.addEventListener('touchmove', this.handleOnMouseMoveBlock);

    window.addEventListener('mousedown', this.handleOnMouseDownBlock);
    window.addEventListener('touchstart', this.handleOnMouseDownBlock);

    window.addEventListener('mouseup', this.handleOnMouseUpBlock);
    window.addEventListener('touchend', this.handleOnMouseUpBlock);
  }

  componentDidMount(){
    let canvas = document.querySelector('#draw_canvas');
    this.setState({canvas: canvas, ctx: canvas.getContext('2d')});
  }

  componentDidUpdate(previousProps, previousState){
    if(previousProps.height !== this.props.height){
      //this.setBrushStyle()
    }
  }

  componentWillUnmount() {
    window.removeEventListener('mousemove', this.handleOnMouseMoveBlock);
    window.removeEventListener('touchmove', this.handleOnMouseMoveBlock);

    window.removeEventListener('mousedown', this.handleOnMouseDownBlock);
    window.removeEventListener('touchstart', this.handleOnMouseDownBlock);

    window.removeEventListener('mouseup', this.handleOnMouseUpBlock);
    window.removeEventListener('touchend', this.handleOnMouseUpBlock);
  }

  midPointBtw = (p1, p2) => {
    return {
      x: p1.x + (p2.x - p1.x) / 2,
      y: p1.y + (p2.y - p1.y) / 2,
    };
  };

  handleOnMouseUpBlock = (e) => {
    points = [];
    this.setState({
      drawing: false
    });
  };

  handleOnMouseDownBlock = (e) => {
    points = [];
    this.setState({
      drawing: true
    });
  };

  handleOnMouseMoveBlock =  (e) => {
    let { appState } = this.props;
    let rect = this.state.canvas.getBoundingClientRect();
    let ctx = this.state.ctx;

    if(this.props.drawOpen && this.state.drawing) {

     // if (this.state.brushType === 1) {
        points.push({
          x: (e.touches ? e.touches[0].clientX : e.clientX) - rect.left,
          y: (e.touches ? e.touches[0].clientY : e.clientY) - rect.top
        });
        let color = this.props.brushColor;
        // ctx.shadowBlur = 0;
        ctx.lineWidth = this.props.brushSize || 30;
        ctx.lineCap = 'round';
        ctx.strokeStyle = color;

        var p1 = points[0];
        var p2 = points[1];

        ctx.beginPath();
        ctx.moveTo(p1.x, p1.y);

        for (var i = 1, len = points.length; i < len; i++) {
          var midPoint = this.midPointBtw(p1, p2);
          ctx.lineWidth = this.state.brushSize;
          ctx.lineCap = 'round';
          ctx.strokeStyle = color;
          ctx.quadraticCurveTo(p1.x, p1.y, midPoint.x, midPoint.y);
          p1 = points[i];
          p2 = points[i + 1];
        }
        ctx.lineTo(p1.x, p1.y);
        ctx.stroke();
    //  }
    }
  };

  setObject = (e) => {
    //console.log('setObject', e);
   // if(!e)return;
    //this.setState({canvas: e, ctx: e.getContext('2d')})
  };

  render() {
    let {appState, height, width} = this.props;

    return (
      <div className='draw'>
        <div className='draw_inner'>
          <canvas ref={this.setObject}
                  width={width ? `${width}px` : '650px'}
                  height={height ? `${height}px` : '560px'}
                  className="draw_canvas" id="draw_canvas">

          </canvas>
        </div>
      </div>
    );
  }
}
