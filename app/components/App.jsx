import React from "react";
import { inject, observer } from "mobx-react";

//components
import SharePage from './SharePage';
import DrawCanvas from './DrawCanvas';
import User from '../stores/User';
import { GET_USER_URL } from '../services/constants';
import { RestApi } from '../services/RestApi';
import { Router, Switch, Route } from 'react-router';
import { createConnection } from '../services/websockets';
import ExampleItem from "./ExampleItem/index";
import ExampleTwoItem from "./ExampleTwoItem/index";
import LandingPage from "./LandingPage/index";

@inject("appState")
@observer
export default class App extends React.Component {
    componentWillMount () {
        window.addEventListener('resize', this.onWindowResize);
        this.onWindowResize();
        this.loadCurrentUser(this.props.appState)
    }

    componentWillUnmount () {
        window.removeEventListener('resize', this.onWindowResize);
    }

    onWindowResize = () => {
        const { appState } = this.props;
        let pageSize;
        let bodyWidth = document.body.offsetWidth;

        if (bodyWidth < 768) {
            pageSize = 'xs';
        } else if (bodyWidth < 992) {
            pageSize = 'sm';
        } else if (bodyWidth < 1200) {
            pageSize = 'md';
        } else {
            pageSize = 'lg';
        }

        if (pageSize !== appState.pageSize) {
            appState.update({ pageSize })
        }
    };

    loadCurrentUser(appState) {
      RestApi({}, GET_USER_URL).then(response => {
        let user = User.create(response.data.data);
        appState.update({
          currentUser: user,
          anonymus: false
        });
        appState.users.push(user);
        appState.currentUser.loadUserWalls();
      }).catch(error => {
        console.log('error', error)
      });
    };

  renderPage = () =>{
    // TODO: Split ChatPage to Layout component and subroutes
    return (
      <div className='page__layout'>
        <div className='page__content'>
        <Switch>
          <Route
            key='main'
            name="main"
            path='/draw'
            component={DrawCanvas}
          />
          <Route
            key='example'
            name="example"
            path='/example'
            component={ExampleItem}
          />
          <Route
            key='exampletwo'
            name="exampletwo"
            path='/exampletwo'
            component={ExampleTwoItem}
          />
          <Route
            key='mains'
            name="mains"
            path='/share'
            component={SharePage}
          />

          <Route
            key='main'
            name="main"
            path='/'
            component={LandingPage}
          />

        </Switch>
        </div>
      </div>
    )
  };

    render() {
        const { appState } = this.props;
        const { router, currentUser} = appState;
        //this.loadCurrentUser(appState);

        return (
        <Router history={router.history}>
          <Switch>
            <Route path='/' render={this.renderPage} />
          </Switch>
        </Router>

        )
    }
}
/*
 <div className="layout">
 <Router state={router}>
 <MainPage route="/" user={currentUser} />
 <SharePage route="/share" user={currentUser} />
 <MainPage route="/add" user={currentUser} />
 <MainPage route="/column" user={currentUser} />
 <MainPage route={/\/!\/(\/(.*))?/} user={currentUser} />
 <LogInPage route="/login" />
 <MainPage route={/\/(\/(.*)\/followers)?/} user={currentUser} />
 <MainPage route={/\/(\/(.*))?/} user={currentUser} />
 </Router>
 </div>
* */