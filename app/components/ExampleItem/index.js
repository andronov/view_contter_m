import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';


@inject("appState")
@observer
export default class ExampleItem extends React.Component {

  state = {
    canvas: null,
    ctx: null,
    canvas_2: null,
    ctx_2: null,
    drawing: false,
    anim: true,
    animation: 'lineRightToLeft',
    canvasText: {
      text: 'Game of Thrones season 7 Bingo cards',
      font: '72px Anton',
      fontFamily: 'Anton',
      fontSize: '32px',
      fillStyle: '#000000'
    },
  };

  componentDidMount(){
    let canvas = document.querySelector(`#ei_canvas`);
    let canvas_2 = document.querySelector(`#ei_canvas_2`);
    this.setState({
      canvas_2: canvas_2, ctx_2: canvas_2.getContext('2d'),
      canvas: canvas, ctx: canvas.getContext('2d')
    });
    setTimeout(()=>{
      this.setAnimationBase()
    }, 10);
  };

  onClickAnim = () => {
    this.setState({anim: true});
    setTimeout(()=>{
      this.setAnimationBase()
    }, 10);
  };

  setAnimationBase = () => {
    this.setAnimation();
  };

  setAnimation = () => {
    let { canvas, ctx, ctx_2, canvasText,  anim} = this.state;
    let { item } = this.props;
    if(!canvas)return;

    //ctx.fillStyle = 'blue';
    let windowW = canvas.width;
    let windowH = canvas.height;

    let bg = new Image();
    bg.src = 'assets/media/2_6_2017/u5l8NfPI6O.jpg';

    bg.onload = function() {
      ctx_2.clearRect(0, 0, windowW, windowH);
      //ctx_2.scale(sc, sc);
      ctx_2.drawImage(bg, 0, 0, windowW, windowH);
    };

    let dashLen = 220, dashOffset = dashLen, speed = 180,
      txt = canvasText.text, x = 30, i = 0;
    ctx.font = `${canvasText.fontSize} ${canvasText.fontFamily}`;
    ctx.lineWidth = 1; ctx.lineJoin = "round";
    ctx.strokeStyle = ctx.fillStyle = canvasText.fillStyle;

    (function loop() {
      ctx.clearRect(x, 0, 60, 150);
      ctx.setLineDash([dashLen - dashOffset, dashOffset - speed]); // create a long dash mask
      dashOffset -= speed;                                         // reduce dash length
      ctx.strokeText(txt[i], x, windowH-50);                               // stroke letter

      if (dashOffset > 0) requestAnimationFrame(loop);             // animate
      else {
        ctx.fillText(txt[i], x, windowH-50);                               // fill final letter
        dashOffset = dashLen;                                      // prep next char
        x += ctx.measureText(txt[i++]).width + ctx.lineWidth;
        //ctx.setTransform(1, 0, 0, 1, 0, 3 * Math.random());        // random y-delta
        //ctx.rotate(Math.random() * 0.005);                         // random rotation
        if (i < txt.length) requestAnimationFrame(loop);
      }
    })();
    /*let w = 2;
    let h = 100;
    let xPos = windowW;
    let that = this;
    let a = anim;

    let i = 0;
    let texts = canvasText.text;

    let bg = new Image();
    bg.src = 'assets/media/2_6_2017/u5l8NfPI6O.jpg';
    let sc = 1;

    let colors = [
      '#00c599',
      '#0073d1',
      '#8500ff',
      '#d10014',
      '#d66822',
      '#f0bf00',
      '#000000',
      '#ffffff'
    ];


    let draw = function () {
      if(!a){
        return;
      }

      ctx_2.clearRect(0, 0, windowW, windowH);
      ctx_2.scale(sc, sc);
      ctx_2.drawImage(bg, 0, 0, windowW, windowH);


      ctx.clearRect(0, 0, windowW, windowH);
      //ctx.translate(incr, 0);
      //ctx.fillStyle = `#${itemLinkColor}`;
      ctx.fillRect(xPos, (windowH-h)-25, w, h);
      // ctx.fill();

      ctx.font = `${canvasText.fontSize} ${canvasText.fontFamily}`;
      ctx.fillStyle = colors[Math.floor(Math.random()*colors.length)];
      //ctx.fillText("Wimbledon: how does the weather", 10, h-70);
      //ctx.fillText("affect things?", 10, h-40);
      ctx.fillText(texts.slice(0, i), xPos+30, windowH-67);

      xPos -= 6;
      i += 1;
      sc += 0.00001;
      //console.log('xPos',  that);
      if(xPos < 30){
        a = false;
        that.setState({anim: false})
      }
      requestAnimFrame(draw)
    };

    bg.onload = function()
    {
      requestAnimFrame(draw);
    };*/

  };


  render() {
    let { colors } = this.state;

    return (
      <div className='ei'>
        <div className='ei' style={{height: '366px', width: '650px'}}>
          <canvas id="ei_canvas" style={{zIndex: 20}}
                  className="ei_canva ei_canvas" height={'366px'} width={'650px'}/>

          <canvas id="ei_canvas_2" style={{zIndex: 10}}
                  className="ei_canva ei_canvas_2" height={'366px'} width={'650px'}/>
        </div>

        <svg width="200" height="200" xmlns="http://www.w3.org/2000/svg">
          <path fill="red" d="M15.699 19.399 L19.398 19.399 L19.898 20.599 C19.998 20.898 20.297 20.998 20.597 20.998 L20.597 20.998 C21.097 20.998 21.496 20.498 21.297 19.998 L18.398 13.299 C18.298 12.999 17.999 12.8 17.6 12.8 L17.6 12.8 C17.301 12.8 17.001 12.999 16.802 13.299 L14.002 20.099 C13.803 20.599 14.103 21.099 14.702 21.099 L14.702 21.099 C15.002 21.099 15.201 20.899 15.4 20.7 L15.7 19.4 L15.7 19.4 M17.6 14.4 L18.999 18.1 L16.2 18.1 L17.599 14.4 L17.599 14.4 M15.699 19.4 Z" />

        </svg>


        <div onClick={this.onClickAnim} >refresh</div>
      </div>
    );
  }
}
