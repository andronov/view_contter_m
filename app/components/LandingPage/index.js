import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import {Motion, spring} from 'react-motion';
import {RestApi} from "../../services/RestApi";
import SharePage from "../SharePage/index";

@inject("appState")
@observer
export default class LandingPage extends React.Component {

  state = {
    canvas: null,
    ctx: null,
    item: null,
    loading: false,
    finishSuccess: true,
    finishError: false,
    itemLinkValue: ''
  };

  onChangeInputLink = (e) => {
    this.setState({itemLinkValue: e.target.value});
  };

  onClickGo = (e) => {
    let { itemLinkValue } = this.state;
    this.setState({loading: true});
    RestApi({link: itemLinkValue}, '').then(response => {
      let item = response.data;
      item['path'] = item.path.split('\\').slice(-4).join('/');
      this.setState({item});
      console.log('response', this.state.itemLink);
    }).catch(error => {
      console.log('error', error)
    });
  };

  onClickLoading = (e) => {
    this.setState({loading: false})
  };

  render() {
    let { itemLinkValue, loading, item } = this.state;

    if(item){
      return(
        <SharePage item={item}/>
      )
    }

    return (
      <div className='lg'>
        <div className='lg__inner'>

          <div className='lg__object' style={{width: '600px', height: '450px'}}>
            {/*<div className="lg__bg"/>
            <img width="600px" src="/assets/media/promo/dfb62.gif"/>*/}

            <div className="lg__form" style={{width: '600px', height: '450px'}}>
              <div className="lg__form_inner">

                <Motion style={{x: spring(loading  ? 0 : 600), y: spring(loading  ? 0 : 100)}}>
                  {({x, y}) =>
                    <div style={{width: `${x}px`, height: `${y}%`}} className="lg__form_block">
                      <div className="lg__form_content">
                        <div className="lg__input">
                          <input
                            placeholder="Paste link"
                            onChange={(e) => this.onChangeInputLink(e)} value={itemLinkValue} className="lg__control"  />
                        </div>
                        <div className="lg__btns">
                          <div onClick={this.onClickGo} className="lg__btn go">Generate link</div>
                        </div>
                      </div>
                    </div>
                  }
                </Motion>

                <Motion style={{x: spring(loading  ? 100 : 100), t: spring(loading  ? 0 : -1)}}>
                  {({x, t}) =>
                    <div style={{zIndex: t}} className="lg__wait">
                      <div className="lg__loading">
                        <div className="loader loader-quart"/>
                        <div className="lg__loading_fav">
                          <img src="https://assets.producthunt.com/assets/favicon-9247d7de492fd8ec2553739d3635dca4.ico"/>
                        </div>
                      </div>
                      <div className="lg__loading_b">
                        <div className="lg__loading_t">
                          We аrе processing this link...
                        </div>
                        <div className="lg__loading_l">
                          {itemLinkValue}
                        </div>
                        <div onClick={this.onClickLoading} className="lg__loading_c">
                          Cancel
                        </div>
                      </div>
                    </div>
                  }
                </Motion>




              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}
