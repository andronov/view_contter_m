import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import colorPalettes from 'color-palettes'
import Tooltip from 'rc-tooltip';
import CanvasItem from "../CanvasItem/index";
import LayoutItem from "../LayoutItem/index";

let requestAnimFrame = function(){
  return (
    window.requestAnimationFrame       ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function(/* function */ callback){
      window.setTimeout(callback, 1000 / 60);
    }
  );
}();

@inject("appState")
@observer
export default class Item extends React.Component {

  state = {
    canvas: null,
    canvasItem: {
      x: 28,
      y: 233,
      w: 450,
      h: 120
    },



    ctx: null,
    canvas_2: null,
    ctx_2: null,
    drawing: false,
    anim: true,
    animCounter: 0,
    height: 0,
    width: 0,
    updater: 0,
    itemLink: {"title":"These LED eyelashes are future fashion - The Verge",
      "description":"Sometimes you see a product and you’re like \"Wow, that’s so futuristic.\" Then you remember you’re living in 2017, 20 years after the date that a sentient AI wiped out most of humanity in the...",
      "path":"/assets/media/3_7_2017/OEb6vuaxeV.jpg",
      "images": [
        '/assets/media/2_7_2017/MH1oPWLBkn.png',
        '/assets/media/2_6_2017/1-9Q3tZ2W1mQxaqMJpmDwoDg.gif',
        '/assets/media/2_6_2017/4jL5DEobtg.jpg',
        '/assets/media/2_6_2017/kCABk5LgyI.jpg',
        '/assets/media/2_6_2017/toidIrMR5x.jpg'
      ],
      animate: true,
      layers: [
        {
          type: 'animation_title_to_mouseout',
          id: 1,
          animation: 'onTitleMouseOut',
          store: {
            canvasText: {
              text: "These LED eyelashes are future fashion",
              font: '32px Calibri',
              fontFamily: 'Calibri',
              fontSize: 32,
              fillStyle: '#ffffff'
            },
            canvasTextDesc: {
              text: "Sometimes you see a product and you’re like \"Wow, that’s so futuristic.\" Then you remember you’re living in 2017, 20 years after the date that a sentient AI wiped out most of humanity in the...",
              font: "14px 'Titillium Web'",
              fontFamily: "'Titillium Web'",
              fontSize: 14,
              fillStyle: '#ffffff'
            },
          }
        },
        /*{
          type: 'gradient_vertical',
          id: 1,
          store: {
          }
        },
        {
          id: 2,
          type: 'text_with_title_with_desc',
          //animation: 'lineRightToLeft',
          store: {
            canvasText: {
              text: "These LED eyelashes are future fashion",
              font: '32px Calibri',
              fontFamily: 'Calibri',
              fontSize: 32,
              fillStyle: '#ffffff'
            },

            canvasTextDesc: {
              text: "Sometimes you see a product and you’re like \"Wow, that’s so futuristic.\" Then you remember you’re living in 2017, 20 years after the date that a sentient AI wiped out most of humanity in the...",
              font: "14px 'Titillium Web'",
              fontFamily: "'Titillium Web'",
              fontSize: 14,
              fillStyle: '#ffffff'
            },
          }
        }*/
       /* {
          type: 'gradient_horizontal',
          id: 1,
          store: {
          }
        },
        {
          id: 2,
          type: 'text_with_title_with_desc',
          with_type: 'gradient_horizontal',
          //animation: 'lineRightToLeft',
          store: {
            canvasText: {
              text: "These LED eyelashes are future fashion",
              font: '32px Calibri',
              fontFamily: 'Calibri',
              fontSize: 32,
              fillStyle: '#ffffff'
            },

            canvasTextDesc: {
              text: "Sometimes you see a product and you’re like \"Wow, that’s so futuristic.\" Then you remember you’re living in 2017, 20 years after the date that a sentient AI wiped out most of humanity in the...",
              font: "14px 'Titillium Web'",
              fontFamily: "'Titillium Web'",
              fontSize: 14,
              fillStyle: '#ffffff'
            },
          }
        }*/
      ],
    },
    itemLinkColors: [],
    itemLinkColor: '2c7dff',
  };

  componentDidMount(){
    //let canvas = document.querySelector('#cnv');
    /*let {item} = this.props;
    item['animate'] = false;
    this.setState({
      itemLink: item,
     // canvas: canvas, ctx: canvas.getContext('2d')
    });*/
    setTimeout(()=>{
     // this.setAnimation()
    }, 10);
  }

  setAnimation = () => {
    let { canvas, ctx, anim, itemLink: item, itemLinkColor,
      height, width, ctx_2, canvas_2 } = this.state;
    if(!canvas)return;

    //ctx.fillStyle = 'blue';
    let windowW = canvas.width;
    let windowH = canvas.height;
    width = windowW;
    height = windowH;
    let w = 2;
    let h = 100;
    let xPos = windowW;
    let that = this;

    let img = new Image();
    img.onload = start;
    let i = 0;
    img.src = item.path;
    let texts = 'Game of Thrones season 7 Bingo cards';


    let draw = function () {
      if(!anim){
        return;
      }
      ctx.clearRect(0, 0, windowW, windowH);
      //ctx.translate(incr, 0);
      //ctx.fillStyle = `#${itemLinkColor}`;
      ctx.fillRect(xPos, (windowH-h)-25, w, h);
     // ctx.fill();

      ctx.font = "24px Calibri";
      ctx.fillStyle = "#ffffff";
      //ctx.fillText("Wimbledon: how does the weather", 10, h-70);
      //ctx.fillText("affect things?", 10, h-40);
      ctx.fillText(texts.slice(0, i), xPos+30, windowH-67);

      xPos -= 6;
      i += 1;
      //console.log('xPos',i % 3);
      if(xPos < 30){
        this.setState({anim: false})
      }
      requestAnimFrame(draw)
    };

    let w_2 = width+300;
    let h_2 = height+300;
    let t_2 = -200;
    let l_2 = -200;

    let draw_2 = function () {
      if(t_2 === 0){
        return;
      }
      //console.log('21', t_2, l_2, w_2, h_2);
      ctx_2.clearRect(0, 0, w_2, h_2);
      ctx_2.drawImage(img, t_2, l_2, w_2, h_2);
      //ctx_2.scale(2,2);
      w_2 -= 3;
      h_2 -= 3;
      t_2 += 2;
      l_2 += 2;

      requestAnimFrame(draw_2)
    };

    function start() {
      requestAnimFrame(draw_2);
      //setInterval(draw, 50);
    }

    requestAnimFrame(draw);
    //this.animate(draw)
  };

  componentDidUpdate(prevProps){
  //  console.log('componentDidUpdate', this.props.item, prevProps.item)
  }

  animate = (func) => {
    requestAnimFrame(this.animate, func);
    console.log('func', func)
  };

  onLoadBasicImg = (e) => {
    let { target } = e;
    console.log('onLoadBasicImg', target.height);
    target.parentNode.parentNode.style.height = `${target.height}px`;
    let cp = new colorPalettes(target.src);
    cp.dominantThree({
      format: 'hex'
    }).then( (result) => {
      console.log(result) // #
      this.setState({itemLinkColors: result, itemLinkColor: result[0],
        width: target.width, height: target.height});
      this.props.setItemLinkColors(result);
      this.props.onLoadBasicImg(target.width, target.height)
    })
  };

  onClickLinkModeAddImage = () => {
    this.fileInput.value = '';
    this.fileInput.click();
  };

  onFileChange = (e) => {
    let { itemLink: item } = this.state;
    let that = this;
    console.log('e.target.files', e.target.files);
    let file = e.target.files[0];

    if (file) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function(e) {
       // console.log(e.target.result);
        item.path = e.target.result;//(window.URL || window.webkitURL).createObjectURL(e.target.result);
        that.setState({itemLink: item})
      };
    }
  };


  onClickLinkModeSelectImage = (e, item, dir) => {
    let { images } = item;
    let img = images[Math.floor(Math.random() * images.length)];
    //console.log('onClickLinkModeSelectImage', img, item, dir);
    item.originalPath = item.path;
    item.path = img;
    this.setState({itemLink: item})
  };

  setFileInput = (ref) => {
    this.fileInput = ref;
  };

  renderAddImageTip = () => {
    return(
      <div className="link_mode__im_tip">
        <div className="link_mode__im_tip_title">
          Uploading link image
        </div>
        <div className="link_mode__im_tip_desc">
          Please use the image with more than 537x240 pixels to make your link more attractive.
          You can upload JPG, GIF or PNG files. Select the image area for publication after uploading, if necessary.
        </div>
      </div>
    )
  };

  renderLinkMode = (item) => {

    return(
      <div className="link_mode">
        <div className="link_mode__inner">
          <div className="link_mode__left">
            <div
              onClick={(e, it) => this.onClickLinkModeSelectImage(e, item, 'left')}
              className="link_mode__btn_left" />
            <div
              onClick={(e, it) => this.onClickLinkModeSelectImage(e, item, 'right')}
              className="link_mode__btn_right" />
            <div onClick={this.onClickLinkModeAddImage}
                 className="link_mode__add_image" >
              <Tooltip placement="top" overlay={this.renderAddImageTip} ><div style={{width: '16px', height: '16px'}}/></Tooltip>
              <input
                style={{ display: 'none' }}
                onChange={this.onFileChange}
                type='file'
                name='file'
                ref={this.setFileInput}
              />
            </div>
          </div>
        </div>
      </div>
    )
  };

  onClickCanvas = (e) => {
    let { canvasItem, ctx } = this.state;
    let rect = e.target.getBoundingClientRect();
    let x = e.clientX - rect.left;
    let y = e.clientY - rect.top;

    ctx.strokeStyle = "rgba(255, 255, 255, 0.5)";
    ctx.lineWidth = 2;

    //ctx.fillStyle = "rgba(255, 255, 255, 0.5)";
    ctx.strokeRect(canvasItem.x, canvasItem.y, canvasItem.w, canvasItem.h);

    console.log('onClickCanvas', x, y, canvasItem)
  };

  onClickAnim = () => {
    this.setState({anim: true, animCounter: this.state.animCounter+1});
    //this.setAnimation()
  };

  setAnim = () => {
    this.setState({anim: false});
  };

  render() {
    let { itemLink: item, updater, itemItemLoading, itemLinkColor: shadowColor } = this.state;
    let { linkOpen, appState } = this.props;

    let width = appState.size.width || 650;

    return(
      <div
        className="object" style={{boxShadow: `0 10px 92px 0 #${shadowColor}`, width: width ? `${width}px` : '650px'}}>
        <div className="object__media">
          <img onLoad={this.onLoadBasicImg}
               width={width ? width : 650}
               className="object__img"
               src={item.path}/>
        </div>

        {<LayoutItem item={item}/>}

        {itemItemLoading && <div className="object__loading"/>}


        {linkOpen && this.renderLinkMode(item)}

        {this.props.children}

        {item.layers.map(layer=>{
          return(
            <CanvasItem
              itemLinkColors={this.state.itemLinkColors}
              type={layer.type}
              animation={layer.animation}
              animCounter={this.state.animCounter}
              setAnim={this.setAnim}
              with_type={layer.with_type}
              anim={this.state.anim}
              store={layer.store}
              name={`cnv_${layer.id}`} item={item}/>
          )
        })}

        {/*<div className="object__block">
          <div className="object__block_info">
            <a target="_blank" className="object__block_title" href="nytimes.com">{item.title}</a>
          </div>
        </div>*/}

        {
          !item.animate ?
          <div className="object__block">
            <div className="object__block_info">
              <a target="_blank" className="object__block_title" href="nytimes.com">{item.title}</a>
              <p target="_blank" className="object__block_desc" >{item.description}</p>
            </div>
          </div>
          : null
        }

        {/*<div className="object__meta">
          <div className="object__meta_info">
            <a target="_blank" href="nytimes.com">nytimes.com</a>
          </div>
        </div>*/}

        {item.animate ? <div onClick={this.onClickAnim} className="object__refresh"/> : null}
      </div>
    );
  }
}
