import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';


@inject("appState")
@observer
export default class Palette extends React.Component {

  state = {
    colors: [
      '#00c599',
      '#0073d1',
      '#8500ff',
      '#d10014',
      '#d66822',
      '#f0bf00',
      '#000000',
      '#ffffff'
    ]
  };
  //#ff1d0f

  render() {
    let { colors } = this.state;

    return (
      <div className='palette'>
        {colors.map(item=>{
          return(
            <div onClick={(e, it) => this.props.onSelectColor(e, item)} className='palette__item' style={{background: item}} />
          )
        })}
      </div>
    );
  }
}
