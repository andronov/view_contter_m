import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';



@inject("appState")
@observer
export default class MainPage extends React.Component {

  state = {
    open: false,
    fontFamily: null,
    fontName: null,
    font: null
  };

  onClickTitle = (e) => {
    this.setState({open: !this.state.open})
  };

  onMouseMoveList = (e) => {
    let list = e.currentTarget;
    let container = list.parentNode;
    let heightDiff = list.offsetHeight / container.offsetHeight,
      relativeY = (e.pageY - container.offsetTop),
      top = relativeY*heightDiff > list.offsetHeight-container.offsetHeight ?
        list.offsetHeight-container.offsetHeight : relativeY*heightDiff;

    //$list.css("top", -top);
  //  console.log('e', list.offsetHeight, container.offsetHeight, top);
    list.style.top = `${-top}px`;
  };

  onClickDropdown = (e, font) => {
    //console.log('onClickDropdown', e, font);
    this.setState({fontName: font.name, fontFamily: font.fontFamily, font: font, open: false});
    this.props.setFontFamily(font)
  };

  render() {
    let { appState, style } = this.props;
    let { open, fontFamily, fontName } = this.state;
    let { fonts } = appState;

    return (
        <div style={style} className={`dropdown ${open ?  '': 'closed'}`}>
          <div onClick={this.onClickTitle}
               style={{fontFamily: fontFamily ? fontFamily : style.fontFamily}}
               className="title">{fontName ? fontName : style.fontFamily.replace("'", '').replace("'", '')}</div>
          <div style={{height: open ? '200px': '0px'}} className="dropdown-menu">
            <ul onMouseMove={this.onMouseMoveList}>
              {fonts.map(font=>{
                return(
                  <li onClick={(e, item) => this.onClickDropdown(e, font)} style={{fontFamily: font.fontFamily}}>{font.name}</li>
                )
              })}
            </ul>
          </div>
        </div>
    );
  }
}
