import * as React from 'react';
import { observer, inject } from 'mobx-react';
import './style.scss';
import ImageContainer from "./ImageContainer/index";

@inject('appState')
@observer
export default class ImgBlur extends React.Component {
  constructor(props){
    super();
    this.state = {
      showPreview: false,
      showLoader: true,
      showImage: false,
      width: null,
      height: null,
      transitionTime: props.transitionTime || 100
    }
  }

  componentWillMount() {
    const { item, width, height, src, ratio, thumb } = this.props;

    this.loadOriginalImage(src);
    if(thumb){
      let thumbs = src.split('.');
      let ext = thumbs.pop();
      thumbs.push(thumb, `.${ext}`);
      let src_thumb = thumbs.join("");

      this.loadPreviewImage(src_thumb);
    }
    else{
      this.loadPreviewImage(src);
    }

  };

  loadImage(src, callback){
    if(src){
      let image = new Image();
      image.height = 30;
      image.onload = ()=>{
        if(callback){
          callback.call(this);
        }
      };
      image.src = src;
    }
  }

  loadPreviewImage(src){
    this.loadImage(src, ()=>{
      this.setState({
        showPreview: !this.state.showImage,
        showLoader: false
      })
    })
  }

  loadOriginalImage(src){
    this.loadImage(src, ()=>{
      this.setState({
        showPreview: false,
        showLoader: false,
        showImage: true
      })
    })
  }

  calculateAspectRatioFit = (srcWidth, srcHeight, maxWidth, maxHeight) => {
    let ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return { width: srcWidth*ratio, height: srcHeight*ratio };
  };

  render(){
    const { width, height } = this.props;
    return <div className="bil-image">
      { this.state.showImage ? <ImageContainer src={this.props.src}
                                               width={width}
                                               height={height}
                                               fullCover={this.props.fullCover}
                                               maxBlurLevel={this.props.maxBlurLevel}
                                               transitionTime={this.state.transitionTime}/>
        : null }
      { this.state.showPreview ? <ImageContainer src={this.props.preview}
                                                 height={height}
                                                 width={width}
                                                 fullCover={this.props.fullCover}
                                                 maxBlurLevel={this.props.maxBlurLevel}/>
        : null }
      { this.state.showLoader ? null : null }
    </div>
  }
}