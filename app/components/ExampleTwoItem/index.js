import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';


@inject("appState")
@observer
export default class ExampleTwoItem extends React.Component {

  state = {
    canvas: null,
    ctx: null,
    canvas_2: null,
    ctx_2: null,
    drawing: false,
    anim: true,
    animation: 'lineRightToLeft',
    canvasText: {
      text: 'Capture of Breda (1590)',
      font: '72px Roboto',
      fontFamily: 'Roboto',
      fontSize: '32px',
      fillStyle: '#ffffff'
    },
    canvasTextDesc: {
      text: "The Capture of Breda or the Siege of Breda was a short battle</>during the Eighty Years' " +
      "War and the Anglo–Spanish War </>during which a Dutch and English army led by Maurice of Nassau </>" +
      "captured the heavily protected city of Breda. </>Using a clever tactic reminiscent of the " +
      "Trojan </>horse a small assault force hid in a peat barge entered the city of Breda </>and proceeded " +
      "to take it over resulting in </>a minimum number of casualties.",
      font: '16px Roboto',
      fontFamily: 'Roboto',
      fontSize: '16px',
      fillStyle: '#ffffff'
    },
  };

  componentDidMount(){
    let canvas = document.querySelector(`#ei_canvas`);
    let canvas_2 = document.querySelector(`#ei_canvas_2`);
    this.setState({
      canvas_2: canvas_2, ctx_2: canvas_2.getContext('2d'),
      canvas: canvas, ctx: canvas.getContext('2d')
    });
    setTimeout(()=>{
      this.setAnimationBase()
    }, 10);
  };

  onClickAnim = () => {
    this.setState({anim: true});
    setTimeout(()=>{
      this.setAnimationBase()
    }, 10);
  };

  setAnimationBase = () => {
    this.setAnimation();
  };

  setAnimation = () => {
    let { canvas, ctx, ctx_2, canvasText,  canvasTextDesc, anim} = this.state;
    let { item } = this.props;
    if(!canvas)return;

    //ctx.fillStyle = 'blue';
    let windowW = canvas.width;
    let windowH = canvas.height;
    let w = 2;
    let h = 100;
    let xPos = windowW;
    let that = this;
    let a = anim;

    let i = 0;
    let texts = canvasText.text;

    let bg = new Image();
    bg.src = 'assets/media/2_6_2017/Turfschip_Breda.jpg';
    let sc = 1;

    let colors = [
      '#00c599',
      '#0073d1',
      '#8500ff',
      '#d10014',
      '#d66822',
      '#f0bf00',
      '#000000',
      '#ffffff'
    ];


    /*let draw = function () {
      if(!a){
        return;
      }

      ctx_2.clearRect(0, 0, windowW, windowH);
      ctx_2.scale(sc, sc);
      ctx_2.drawImage(bg, 0, 0, windowW, windowH);

      ctx_2.fillStyle = "rgba(0, 0, 0, 0.5)";
      ctx_2.fillRect(0, 0, windowW, windowH);


      ctx.clearRect(0, 0, windowW, windowH);
      //ctx.translate(incr, 0);
      //ctx.fillStyle = `#${itemLinkColor}`;
      ctx.fillRect(xPos, (windowH-h)-25, w, h);
      // ctx.fill();

      ctx.font = `${canvasText.fontSize} ${canvasText.fontFamily}`;
      ctx.fillStyle = "red";
      //ctx.fillText("Wimbledon: how does the weather", 10, h-70);
      //ctx.fillText("affect things?", 10, h-40);
      ctx.fillText(texts.slice(0, i), xPos+30, windowH-67);

      xPos -= 6;
      i += 1;
      sc += 0.00001;
      //console.log('xPos',  that);
      if(xPos < 30){
        a = false;
        that.setState({anim: false})
      }
      requestAnimFrame(draw)
    };*/

    bg.onload = function()
    {
      ctx_2.clearRect(0, 0, windowW, windowH);
      ctx_2.scale(sc, sc);
      ctx_2.drawImage(bg, 0, 0, windowW, windowH);

      ctx_2.fillStyle = "rgba(0, 0, 0, 0.5)";
      ctx_2.fillRect(0, 0, windowW, windowH);


      ctx.fillStyle = "rgba(0, 0, 0, 0.5)";
      ctx.fillRect(10, windowH/2-50, windowW-20, 260);



      ctx.font = `${canvasText.fontSize} ${canvasText.fontFamily}`;
      ctx.fillStyle = "white";
      ctx.fillText(texts, 90, windowH/2);

      let i = 30;
      canvasTextDesc.text.split('</>').forEach((text)=>{
        ctx.font = `${canvasTextDesc.fontSize} ${canvasTextDesc.fontFamily}`;
        ctx.fillStyle = "white";
        ctx.fillText(text, 30, windowH/2+10+i);
        i += 20;
      });

    };

  };


  render() {
    let { colors } = this.state;

    return (
      <div className='ei'>
        <div className='ei' style={{height: '450px', width: '559px'}}>
          <canvas id="ei_canvas" style={{zIndex: 20}}
                  className="ei_canva ei_canvas" height={'450px'} width={'559px'}/>

          <canvas id="ei_canvas_2" style={{zIndex: 10}}
                  className="ei_canva ei_canvas_2" height={'450px'} width={'559px'}/>
        </div>

        <div onClick={this.onClickAnim} >refresh</div>
      </div>
    );
  }
}
