// eslint-disable-next-line
import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom'
import './style.scss';

/**
 * Константы состояния стикера
 * @type {{NORMAL: number, MOVE: number, TRANSFORM: number}}
 */
const m = {
  NORMAL:0, //нормальное
  MOVE:1, // перемещение
  TRANSFORM:2 // трансформация
};


class ElementItem extends Component {
  static propTypes = {
    image: PropTypes.string, //Урл картинки
    top: PropTypes.number,
    left: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
    rotate: PropTypes.number, //угол поворота в градусах
    onChange: PropTypes.func
  };

  state = {
    selected: false,
    mode:m.NORMAL,
    element: null,
    mouseX:0,
    mouseY:0,
    top:this.props.top || 100,
    left: this.props.left || 500,
    height: this.props.height || 100,
    width: this.props.width || 100,
    rotate: this.props.rotate || 0,
    corner:null,
    image: this.props.image || '',
    onChange: this.props.onChange || null,
  };

  /**
   * при содании компонента инициализируются события связанные с родителем
   */
  componentDidMount(){
    const self = this;
    const parent = ReactDOM.findDOMNode(this).parentNode;

    /*let img = new Image();
    img.src = this.props.image;
    img.onload = function () {
      console.log('componentDidMount', self.props.image, img.height, img.width);
      self.setState({height: img.height, width: 150})
    };*/

   /* parent.addEventListener('mousedown', function () {
      self.setState({
        selected: false
      })
    });*/

    parent.addEventListener('mouseup', function () {
      self.setState({
        mode:m.NORMAL
      })
    });

    parent.addEventListener('mousemove', function (e) {
      self.mouseMoveHandler.call(self, e)
    });


   parent.addEventListener('touchstart', function () {
      self.setState({
        selected: false
      })
    });

   /* parent.addEventListener('touchend', function () {
      self.setState({
        mode:m.NORMAL
      })
    });*/

    parent.addEventListener('touchmove', function (e) {
      self.mouseMoveHandler.call(self, e)
    })
  }

  componentWillUnmount() {
    const self = this;
    const parent = ReactDOM.findDOMNode(this).parentNode;

  /*  parent.removeEventListener('mousedown', function () {
      self.setState({
        selected: false
      })
    });*/

    parent.removeEventListener('mouseup', function () {
      self.setState({
        mode:m.NORMAL
      })
    });

    parent.removeEventListener('mousemove', function (e) {
      self.mouseMoveHandler.call(self, e)
    });

    parent.removeEventListener('touchstart', function () {
      self.setState({
        selected: false
      })
    });

   /* parent.removeEventListener('touchend', function () {
      self.setState({
        mode:m.NORMAL
      })
    });*/

    parent.removeEventListener('touchmove', function (e) {
      self.mouseMoveHandler.call(self, e)
    })
  }

  /**
   * При обновлении компонента инициализируются событие связанные с маркерами углов
   */
  componentDidUpdate(){
    const self = this;
    const el = ReactDOM.findDOMNode(this);
    const coners = el.querySelectorAll('.element_item.selected .corner');
    coners.forEach(function (item) {
      item.addEventListener('mousedown', function (e) {
        self.conerMouseDownHandler.call(self, e)
      });

      item.addEventListener('mouseup', function (e) {
        self.conerMouseUpHandler.call(self, e)
      })
    })
  }

  /**
   * Обработчик события нажатия кнопки мыши на маркере угла (старт трансформации)
   * @param e
   */
  conerMouseDownHandler(e){
    e.stopPropagation();

    this.setState({
      mode: m.TRANSFORM,
      selected: true,
      corner: e.currentTarget.getAttribute('data')
    })
  }

  /**
   * Отпускание кнопки мыши на маркере угла (конец трансформаций)
   * @param e
   */
  conerMouseUpHandler(e){
    e.stopPropagation();
    /*this.setState({
      mode: m.NORMAL,
      selected: true,
    })*/
  }

  /**
   * Нажатие кнопки мыши на стикере для старта перетаскивания
   * @param e
   */
  mouseDownHandler(e){
    console.log('e.touches', e.touches);
    if(!e.touches)return;
    e.stopPropagation();
    this.setState({
      mode: m.MOVE,
      selected: true,
      mouseX: e.touches[0].pageX,
      mouseY:e.touches[0].pageY
    });
    this.props.onActiveItem(e, this.props.item)
  }

  /**
   * Отпускание кнопки мыши на стикере конец перетаскивания
   * @param e
   */
  mouseUpHandler(e){
    e.stopPropagation();
    let {element} = this.state;

    let trash = document.querySelector('.btn_trash');
    let rect = element.getBoundingClientRect();
    let rect_t = trash.getBoundingClientRect();
    if(rect.top < rect_t.height){
      this.props.onClickTrash()
    }

   /* this.setState({
      mode:m.NORMAL
    })*/
  }

  /**
   * Перемещение мышки над контейнером в котором стикер
   * @param e
   */
  mouseMoveHandler(e){
    if(!this.state.selected)return;
    const parent = ReactDOM.findDOMNode(this).parentNode;
    const {selected,  mouseY, mouseX, corner, element} = this.state;
    let {mode, top, left, width, height} = this.state;
    if(!e.touches)return;
    //console.log('eeeee', e.touches, top, left, mode, selected);
    let m_mode = m.MOVE;
    if(e.touches.length === 2){
      m_mode = m.TRANSFORM
    }

    if (m_mode === m.MOVE && selected) {//Перетаскивание
      let dx = e.touches[0].pageX - mouseX;
      let dy = e.touches[0].pageY - mouseY;
      top = (top+dy);
      left = (left+dx);

      let trash = document.querySelector('.btn_trash');
      let rect = element.getBoundingClientRect();
      let rect_t = trash.getBoundingClientRect();
      if(rect.top < rect_t.height){
        trash.classList.add('active');
      }
      else{
        trash.classList.remove('active');
      }

      this.setState({
        mode:mode,
        mouseX: e.touches[0].pageX,
        mouseY: e.touches[0].pageY,
        top:top,
        left:left
      });

      this.onChangeHandler({
        width: this.state.width,
        height: this.state.height,
        top:top,
        left:left,
        rotate: this.state.rotate,
        image: this.state.image
      })
    } else if (m_mode === m.TRANSFORM && selected) { //Трасформация
      let xy = [left, top];
      let size = [width, height];
      let centerXY = [xy[0] + size[0] / 2, xy[1] + size[1] / 2];
      let dXY = [];

     /* // eslint-disable-next-line
      switch (corner) {
        case "se":
          dXY = [xy[0] + size[0] - centerXY[0], xy[1] + size[1] - centerXY[1]];
          break;
        case "sw":
          dXY = [xy[0] - centerXY[0], xy[1] + size[1] - centerXY[1]];
          break;
        case "ne":
          dXY = [xy[0] + size[0] - centerXY[0], xy[1] - centerXY[1]];
          break;
        case "nw":
          dXY = [xy[0] - centerXY[0], xy[1] - centerXY[1]]
      }

      let dXY_Length = Math.sqrt(dXY[0] * dXY[0] + dXY[1] * dXY[1]);
      let offset = [e.pageX-parent.offsetLeft, e.pageY-parent.offsetTop];
      xy = [offset[0] - centerXY[0], offset[1] - centerXY[1]];
      let angle = 180 * (Math.atan2(xy[1], xy[0]) - Math.atan2(dXY[1], dXY[0])) / Math.PI;
      let xy_Length = Math.max(30, Math.sqrt(xy[0] * xy[0] + xy[1] * xy[1]));
      let p = xy_Length / dXY_Length;
      width = size[0] * p;
      height = size[1] * p;*/

      let rotation = e.rotation;
      console.log('rotation', rotation);

      // This isn't a fun browser!
      //if ( ! rotation) {
        rotation = Math.arctan2(e.touches[0].pageY - e.touches[1].pageY,
            e.touches[0].pageX - e.touches[1].pageX) * 180 / Math.PI;
      //}

      // Take into account vendor prefixes, which I haven't done.
    //  this.style.transform = "rotate(" + rotation + "deg)";

      //Можно раскоментировать и будет ограничение по размерам для стикеров
      //width = Math.min(260, width)
      //height = Math.min(260, height)

      this.setState({
        width: width,
        height: height,
        left: centerXY[0] - width / 2,
        top: centerXY[1] - height / 2,
        rotate: rotation
      });

      this.onChangeHandler({
        width: width,
        height: height,
        left: centerXY[0] - width / 2,
        top: centerXY[1] - height / 2,
        rotate: rotation,
        image: this.state.image
      })
    }

  }

  /**
   * Возвращает измененные настройки стикера вызывая переданный колбек, если есть
   * @param data
   */
  onChangeHandler(data){
    const {onChange} = this.state;

    if (typeof onChange === 'function'){
      onChange(data)
    }
  }

  /**
   * Генерирует массив дом элменетов маркеров углом для трансформации
   * @returns {*}
   */
  getMarkers(){
    const {selected} = this.state;
    if (!selected) { return null}
    const coners = ['ne', 'se', 'sw', 'nw'];
    const components = [];
    let cc = null;
    for (let i=0, cnt=coners.length; i< cnt; i++){
      cc = ['corner', coners[i]].join(' ');
      components.push(<div data={coners[i]} className={cc} key={i}/>);
      cc = null
    }
    return components
  }

  setItemLayout = (e) => {
    if(!e)return;
    if(!this.state.element){
      this.setState({element: e})
    }
    e.style.zIndex = this.props.setItemLayout(e);
  };

  /**
   * Генерирует компонент
   * @returns {XML}
   */
  render() {
    const {selected, top, left, height, width, rotate, image} = this.state;
    const { className, style: s } = this.props;
    const classes = ['element_item', className, (selected ? 'selected' : '') ].join(' ');
    const style = {
      backgroundImage: `url('${image}')`,
      top: top,
      left: left,
      height: height,
      width: width,
      transform: "rotateZ(" + rotate + "deg)",
      ...s
    };

    return <div
      ref={this.setItemLayout}
      style={style}
      className={classes}
      onMouseDown={this.mouseDownHandler.bind(this)}
      onMouseUp={this.mouseUpHandler.bind(this)}
      onTouchStart={this.mouseDownHandler.bind(this)}
      onTouchEnd={this.mouseUpHandler.bind(this)}
    >
      {this.getMarkers()}
    </div>
  }
}

export default ElementItem