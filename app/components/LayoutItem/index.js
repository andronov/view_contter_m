import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import range from 'lodash.range';
import {Motion, spring} from 'react-motion';

function reinsert(arr, from, to) {
  const _arr = arr.slice(0);
  const val = _arr[from];
  _arr.splice(from, 1);
  _arr.splice(to, 0, val);
  return _arr;
}

function clamp(n, min, max) {
  return Math.max(Math.min(n, max), min);
}

const springConfig = {stiffness: 30, damping: 5};

@inject("appState")
@observer
export default class LayoutItem extends React.Component {

  state = {
    topDeltaX: 0,
    mouseX: 0,
    isPressed: false,
    originalPosOfLastPressed: 0,
    order: range(2),
  };

  componentDidMount() {
    window.addEventListener('touchmove', this.handleTouchMove);
    window.addEventListener('touchend', this.handleMouseUp);
    window.addEventListener('mousemove', this.handleMouseMove);
    window.addEventListener('mouseup', this.handleMouseUp);

    this.setState({order: range(this.props.appState.itemLayouts.length)})
  };

  componentDidUpdate(prevProps){
    let itemsCount = this.props.appState.itemLayouts.length;
    if(this.state.order.length !== itemsCount ){
      this.setState({order: range(this.props.appState.itemLayouts.length)})
    }

   // console.log('itemsCount', itemsCount, this.state.order)
  }

  handleTouchStart = (key, pressLocation, e) => {
    this.handleMouseDown(key, pressLocation, e.touches[0]);
  };

  handleTouchMove = (e) => {
   // e.preventDefault();
    this.handleMouseMove(e.touches[0]);
  };

  handleMouseDown = (pos, pressX, {pageX}) => {
    this.setState({
      topDeltaX: pageX - pressX,
      mouseX: pressX,
      isPressed: true,
      originalPosOfLastPressed: pos,
    });
  };

  handleMouseMove = ({pageX}) => {
    const {isPressed, topDeltaX, order, originalPosOfLastPressed} = this.state;
    let itemsCount = this.props.appState.itemLayouts.length;

    if (isPressed) {
      const mouseX = pageX - topDeltaX;
      const currentRow = clamp(Math.round(mouseX/30), 0, itemsCount - 1);
      let newOrder = order;

      if (currentRow !== order.indexOf(originalPosOfLastPressed)){
        newOrder = reinsert(order, order.indexOf(originalPosOfLastPressed), currentRow);
      }

      this.setState({mouseX: mouseX, order: newOrder});
      this.props.appState.updateItemLayouts(this.state.order);
    }
  };

  handleMouseUp = () => {
    this.setState({isPressed: false, topDeltaX: 0});
    //this.props.appState.updateItemLayouts(this.state.order);
    //console.log('up',  this.state.order, this.props.appState.itemLayouts)
  };


  render() {
    const {mouseX, isPressed, originalPosOfLastPressed, order} = this.state;
    let { appState } = this.props;
    let { itemLayouts } = appState;
    let itemsCount = itemLayouts.length;
    //console.log('render7render', this, itemsCount, order);

    return (
      <div className='obj_layout'>
        <div className='obj_layout__list'>

          {/*<div className='obj_layout__item'>
            1
          </div>

          <div className='obj_layout__item '>
            2
          </div>

          <div className='obj_layout__item active'>
            3
          </div>*/}

          {range(itemsCount).map(i => {
            const style = originalPosOfLastPressed === i && isPressed
              ? {
                scale: spring(1.1, springConfig),
                shadow: spring(16, springConfig),
                x: mouseX,
              }
              : {
                scale: spring(1, springConfig),
                shadow: spring(1, springConfig),
                x: spring(order.indexOf(i)*30, springConfig),
              };
            return (
              <Motion style={style} key={i}>
                {({scale, shadow, x}) =>
                  <div
                    onMouseDown={this.handleMouseDown.bind(null, i, x)}
                    onTouchStart={this.handleTouchStart.bind(null, i, x)}
                    className="obj_layout__item"
                    style={{
                      boxShadow: `rgba(0, 0, 0, 0.2) 0px ${shadow}px ${2 * shadow}px 0px`,
                      transform: `translate3d(${x}px, 0, 0) scale(${scale})`,
                      WebkitTransform: `translate3d(${x}px, 0, 0) scale(${scale})`,
                      zIndex: i === originalPosOfLastPressed ? 99 : i,
                    }}>
                    {order.indexOf(i) + 1}
                  </div>
                }
              </Motion>
            );
          })}

        </div>
      </div>
    );
  }
}
