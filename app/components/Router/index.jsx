import React from 'react';
import { inject, observer } from 'mobx-react';
import qs from 'qs';

@observer
export default class Router extends React.Component {
  componentWillMount() {
    const { history } = this.props.state;
    this._unlisten = history.listen(this.updateLocation);
    this.updateLocation(history.location);
  }

  componentWillUnmount() {
    this._unlisten();
  }

  getMatches = currentComponent => (
    currentComponent.props.route.toString().indexOf('(') &&
    this.props.state.path.match(currentComponent.props.route)
  );

  updateLocation = location => {
    this.props.state.update({
      path: location.pathname,
      query: qs.parse(location.search.slice(1))
    });
  };

  render() {
    const { children, state } = this.props;
    const currentComponent = state.path && Array.from(children)
    .filter(Boolean)
    .find(child => (child.props.route || state.path).toString().indexOf('(') > -1
      ? state.path.match(child.props.route)
      : (
        (child.props.route || state.path) === state.path &&
        JSON.stringify(child.props.query || state.query) === JSON.stringify(state.query)
      )
    );

    return currentComponent
      ? React.cloneElement(currentComponent, {
        matches: this.getMatches(currentComponent) || []
      })
      : (
        <h1>no routes</h1>
      )
  }
}
