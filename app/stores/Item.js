import { observable, computed } from 'mobx';
import BaseClass from './BaseClass';
import User from './User';
import Wall from './Wall';
import Site from './Site';
import moment from 'moment';
import { SET_ITEM_LIKE,
  SET_RATING_ITEM } from '../services/constants';
import { RestApi } from '../services/RestApi';

export default class Item extends BaseClass {
  static table = 'items';

  static schema() {
    return {
      user: {
        type: 'one',
        inverse: 'items',
        factory: User,
      },
      wall: {
        type: 'one',
        inverse: 'items',
        factory: Wall,
      },
      site: {
        type: 'one',
        inverse: 'sites',
        factory: Site,
      }
    };
  }

  @observable id;
  @observable unique_id;
  @observable link_id;
  @observable title;
  @observable description;
  @observable small_img;
  @observable medium_img;
  @observable large_img;
  @observable url_path;
  @observable url;
  @observable thumbnail;
  @observable type;
  @observable site;
  @observable created;
  @observable wall_id;
  @observable wall;
  @observable wallType;
  @observable liked;
  @observable json_img;
  @observable site_id;
  @observable shared;
  @observable bookmark = false;
  @observable reportOpen = false;
  @observable bookmarkOpen = false;
  @observable read_browser;
  @observable user;
  $container = false;

  loadedThumb = false;

  @computed get img() {
    // TODO сделать проверку на ошибку
    let img = new Image();
    img.src = `https://s3-us-west-2.amazonaws.com/contter/${this.medium_img}`;
    return img.src

  }

  fromNow() {
    return moment.unix(this.created).fromNow()
  };

  @computed get thumb_img() {
    let img = new Image(`https://s3-us-west-2.amazonaws.com/contter/${this.thumbnail}`);
    return img.src
  }

  async setLike() {
    RestApi({id: this.unique_id}, SET_ITEM_LIKE).then(response => {
      this.liked = response.data.liked
    }).catch(error => {
      console.log('error', error)
    });
  }

  async setRating(type, action) {
    RestApi({type: type, action: action,
    id: this.unique_id}, SET_RATING_ITEM).then(response => {
      //console.log('setRating', type !== action, type === action && this.rating.your !== type, this.rating.your, type, action);
      if(response.data.ok){
        if(type !== action){
          this.rating.update({
            ...this.rating,
            your: 'upvoted',
            score: type === 'up' ?
              this.rating.score-1 : this.rating.score+1
          });
        }
        if(type === action && this.rating.your !== type){
          this.rating.update({
            ...this.rating,
            your: type,
            score: type === 'up' ?
              this.rating.score+(this.rating.your === 'down' ? 2 : 1)
              : this.rating.score-(this.rating.your === 'up' ? 2 : 1)
          });
        }
      }
    }).catch(error => {
      console.log('error', error)
    });
  }

  arcImage = (maxWidth, maxHeight) =>{
    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
      var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
      return { width: srcWidth*ratio, height: srcHeight*ratio };
    }
    if(!this.json_img){return { width: 600, height: 300 }}
    let w = this.json_img.medium_img ? this.json_img.medium_img.width : 600;
    let h = this.json_img.medium_img ? this.json_img.medium_img.height : 1200;
    let mh = maxHeight ? maxHeight : 1500;
    let mw = maxWidth ? maxWidth : 600;
    return calculateAspectRatioFit(w, h, mw, mh)
  }
}