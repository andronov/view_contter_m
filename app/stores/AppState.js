import { observable, computed } from 'mobx';
import BaseClass from './BaseClass';
import uuid from 'uuid';

import Router from './Router';

class AppState extends BaseClass {
  router = Router.create();

  @observable currentUser;
  @observable users = [];

  @observable itemLayouts = [
  ];

  fonts = [
    {
      name: 'Calibri',
      fontFamily: 'Calibri'
    },
    {
      name: 'Helvetica',
      fontFamily: 'Helvetica'
    },
    {
      name: 'Open sans',
      fontFamily: 'Open sans'
    },
    {
      name: 'Titillium Web',
      fontFamily: "'Titillium Web'"
    },
    {
      name: 'Playfair Display',
      fontFamily: "'Playfair Display'"
    },
    {
      name: 'Anton',
      fontFamily: "'Anton"
    },
    {
      name: 'Roboto',
      fontFamily: "'Roboto'"
    }
  ];

  textAnimations = [
    {
      id: 1
    }
  ];

  setItemLayout = (e) => {
    let items = this.itemLayouts.sort((a, b) => a.zIndex- b.zIndex);
    let zIndex = 100;
    if(this.itemLayouts.length){
      zIndex = items[items.length-1].zIndex + 5
    }
    ///console.log('setItemLayout', zIndex, this.itemLayouts);
    let id = uuid.v4();
    let dt = {
      id,
      node: e,
      zIndex: zIndex
    };
    //this.update({itemLayouts: dt});
    this.itemLayouts.push(dt);
    return zIndex
  };

  updateItemLayouts = (order) => {
    let elms = [];
    let zIndex = 100;
    order.forEach(i=>{
      let it = this.itemLayouts.filter((a, b)=>b === i).slice(-1)[0];
      it.zIndex = zIndex;
      it.node.style.zIndex = zIndex;
      zIndex += 5;
      elms.push(it)
    });
   // console.log('updateItemLayouts', elms, order, this.itemLayouts);
  };


  to_rgba = (hex, opacity) => {
    let rgb = hex.replace('#', '').match(/(.{2})/g);

    let i = 3;
    while (i--) {
      rgb[i] = parseInt(rgb[i], 16);
    }

    if (typeof opacity === 'undefined') {
      return 'rgb(' + rgb.join(', ') + ')';
    }

    return 'rgba(' + rgb.join(', ') + ', ' + opacity + ')';
  };

  is_mobile = () => {
    return true
  };

  @computed get size() {
    return {width: window.innerWidth, height: window.innerHeight}
  }

}

export default AppState.create();
