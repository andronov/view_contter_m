import { observable } from 'mobx';
import BaseClass from './BaseClass';
import AppState from './AppState';
import qs from 'qs';

export default class Router extends BaseClass {
  setHistory(history) {
    this.history = history;
  }

  @observable path;
  @observable query = {};

  navigate = (path = '', query) => {
    if (!this.history) {
      throw new Error('you must set history by #setHistory');
    }

    this.history.push({
      pathname: path,
      search: `?${qs.stringify(query)}`,
    });
    console.log('navigate', path, query);
    if(path.startsWith('/!/')){
      AppState.app_content = 'wall'
    }
    else if(path == '/add' || path == '/add/')(
      AppState.app_content = 'add_wall'
    );

    this.update({ path, query });
  }
}
