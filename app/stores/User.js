import { observable, computed } from 'mobx';
import BaseClass from './BaseClass';
import Wall from './Wall';
import Item from './Item';
import appState from './AppState';
import { GET_USER_WALLS_URL,
  GET_ITEMS_URL, ADD_WALL,
  GET_USER_ALL_WALLS,
  SET_WALL_SORTING,
  GET_SUGGEST_USER,
  GET_USER_FOLLOWERS} from '../services/constants';
import { RestApi } from '../services/RestApi';

export default class User extends BaseClass {
  static table = 'users';

  static schema() {
    return {
      walls: {
        type: 'many',
        inverse: 'user',
        factory: Wall,
      },
      items: {
        type: 'many',
        inverse: 'user',
        factory: Item,
      }
    };
  }

  @observable id;
  @observable username;
  @observable types;
  @observable twitter_img;
  @observable twitter;
  @observable newy;
  @observable name;
  @observable last_name;
  @observable first_name;
  @observable website;
  @observable followersCount;
  @observable email;
  @observable display_name;
  @observable is_site;
  @observable description;
  @observable color;
  @observable blocked;
  @observable block_you;
  @observable avatar;
  @observable type;
  @observable short_url;
  @observable UserType;
  @observable walls = [];
  @observable items = [];
  @observable suggestUsers = [];
  @observable followers= [];

  constructor() {
    super();
  }

  @observable activeWall = null;
  /*@computed get activeWall() {
    if(!window.matchRoute)return null;

    if(window.matchRoute.path.startsWith('/:username')) {
      let slug = window.matchRoute.params.slugwall;
      let walls = this.walls.filter(wall => wall.slug == slug);
      return walls.length && walls[0]
    }
  }*/

  @computed get get_ava() {
    let img = new Image(`https://s3-us-west-2.amazonaws.com/contter/${this.avatar}`);
    return `https://s3-us-west-2.amazonaws.com/contter/${this.avatar}`; //img.src
  }

  async loadUserWalls() {
    RestApi({}, GET_USER_WALLS_URL).then(response => {
      response.data.walls.slice().forEach(wl => {
        Wall.create({...wl, user: this, originalSources: wl.sources});
      });
    }).catch(error => {
      console.log('error', error)
    });
  }

  async setSortWalls(data) {
    RestApi(data, SET_WALL_SORTING).then(response => {
      console.log('setSortWalls', response.data)
    }).catch(error => {
      console.log('error', error)
    });
  };

  async loadSuggestUsers() {
    RestApi({id: this.id, short_url: this.short_url},
      GET_SUGGEST_USER).then(response => {
      response.data.users.slice(0, 10).forEach(site => {
        let user = User.create({...site});
        appState.users.push(user);
        this.suggestUsers.push(user);
      });
      console.log('loadSuggestUsers', response.data)
    }).catch(error => {
      console.log('error', error)
    });
  }

  async loadAllUserWalls() {
    console.log('loadAllUserWalls', GET_USER_ALL_WALLS);
    RestApi({username: this.username}, GET_USER_ALL_WALLS).then(response => {
      response.data.walls.slice().forEach(wl => {
        Wall.create({...wl, user: this, originalSources: wl.sources});
      });
    }).catch(error => {
      console.log('error', error)
    });
  }


  async getFollowers(offset, limit) {
    RestApi({
      username: this.username,
      offset: offset,
      limit: limit
    }, GET_USER_FOLLOWERS).then(response => {
      console.log('getFollowers', response.data);
      let fls = [];
      response.data.followers.forEach(fl=> {
        fls.push(User.create({...fl}));
      });
      this.update({followers: fls, followersCount: response.data.followersCount})
    }).catch(error => {
      console.log('error', error)
    });
  }

  async loadItems(slugwall, offset, limit, ids, item) {
    appState.loadingProfile = false;
    RestApi({
      username: this.username,
      slugwall: slugwall,
      offset: offset,
      limit: limit,
      ids: ids
    }, GET_ITEMS_URL).then(response => {
      console.log('loadItems', response.data);
      if(item){item.update({loadedItems: true,
        loadingItems: false,
        lastOffset: offset,
        lastLimit: limit})}
      appState.loadingGlobalItems = false;
      response.data.items.slice().forEach(it => {
        Item.create({...it, user: this, wall: it.wall_id})
      });
    }).catch(error => {
      appState.loadingGlobalItems = false;
      if(item){item.update({loadedItems: true,
        loadingItems: false,
        lastOffset: offset,
        lastLimit: limit})}
      console.log('error', error)
    });
  }

  async addWall(data) {
    return RestApi(data, ADD_WALL).then(response => {
      console.log('addWall', response.data);
      appState.savingAddWall = false;
      let wl = Wall.create({...response.data.data, originalSources: response.data.data.sources});
      this.walls.unshift(wl);
      return wl
    }).catch(error => {
      appState.errorAddWall = true;
      console.log('error', error)
    });
  }

}
